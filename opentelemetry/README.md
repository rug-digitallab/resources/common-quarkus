# OpenTelemetry
The OpenTelemetry library provides a set of utilities for working with OpenTelemetry in Quarkus applications. Specifically,
it provides utility functions seamlessly integrate Quarkus OpenTelemetry and SmallRye Mutiny with Kotlin Coroutines.

## Prerequisites

- Quarkus OpenTelemetry

## Usage

### withCoroutineSpan

The `withCoroutineSpan` utility function creates a new OpenTelemetry span and executes the provided block of code within 
the span. The span is automatically closed when the block of code completes.

```kotlin
suspend fun mySuspendFun() = withCoroutineSpan("MySpan") {
    // Do something
}
```

### contextualUni

The `contextualUni` utility functions converts a suspend function to a Uni, while preserving the OpenTelemetry context. 

```kotlin
fun myUniFun(): Uni<String> = contextualUni {
    mySuspendFun()
}

suspend fun mySuspendFun(): String {
    // Do something
}
```
