package nl.rug.digitallab.common.quarkus.opentelemetry

import io.opentelemetry.api.GlobalOpenTelemetry
import io.opentelemetry.api.trace.Span
import io.opentelemetry.api.trace.SpanBuilder
import io.opentelemetry.api.trace.StatusCode
import io.opentelemetry.api.trace.Tracer
import io.opentelemetry.context.Context
import io.opentelemetry.extension.kotlin.asContextElement
import io.quarkus.opentelemetry.runtime.config.build.OTelBuildConfig.INSTRUMENTATION_NAME
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.coroutines.uni
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * Wrapper function that does not depend on the Tracer object to simplify usage
 *
 * Inspired from https://blog.danielcorreia.net/opentelemetry-kotlin-coroutines/
 *
 * @param spanName The name of the span to start.
 * @param coroutineContext The coroutine context to run the block in.
 * @param parameters The parameters to pass to the span builder.
 * @param block The block of code to execute within the span.
 *
 * @return The result of the executed code.
 */
suspend fun <T> withCoroutineSpan(
    spanName: String,
    coroutineContext: CoroutineContext = EmptyCoroutineContext,
    parameters: (SpanBuilder.() -> Unit)? = null,
    block: suspend CoroutineScope.(Span) -> T,
): T = GlobalOpenTelemetry.getTracer(INSTRUMENTATION_NAME).startCoroutineSpan(spanName, coroutineContext, parameters, block)

/**
 * Starts a span, executes the block, and ends the span.
 *
 * @param spanName The name of the span to start.
 * @param coroutineContext The coroutine context to run the block in.
 * @param parameters The parameters to pass to the span builder.
 * @param block The block of code to execute within the span.
 *
 * @return The result of the executed code.
 */
suspend fun <T> Tracer.startCoroutineSpan(
    spanName: String,
    coroutineContext: CoroutineContext = EmptyCoroutineContext,
    parameters: (SpanBuilder.() -> Unit)? = null,
    block: suspend CoroutineScope.(Span) -> T,
): T {
    val span: Span = this.spanBuilder(spanName).run {
        if (parameters != null) parameters()
        startSpan()
    }

    return withContext(coroutineContext + span.asContextElement()) {
        try {
            block(this, span)
        } catch (throwable: Throwable) {
            span.setStatus(StatusCode.ERROR)
            span.recordException(throwable)
            throw throwable
        } finally {
            span.end()
        }
    }
}

/**
 * Converts a suspend function to a Uni, while preserving the OpenTelemetry context. This ensures that the span
 * is propagated correctly to the coroutine scope. This is required because the Uni is executed on a different
 * thread than the one that created it.
 *
 * @param context The context to run the suspend function in.
 * @param suspendSupplier The suspend function to convert.
 *
 * @return The Uni that executes the suspend function and preserves the OpenTelemetry context.
 */
@ExperimentalCoroutinesApi
@DelicateCoroutinesApi
@OptIn(ExperimentalCoroutinesApi::class)
fun <T> contextualUni(context: CoroutineContext = EmptyCoroutineContext, suspendSupplier: suspend () -> T): Uni<T> =
    uni(CoroutineScope(context + Context.current().asContextElement()), suspendSupplier)
