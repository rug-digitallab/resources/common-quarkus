plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    val openTelemetryKotlinVersion: String by project

    api("io.quarkus:quarkus-opentelemetry")

    implementation("io.opentelemetry:opentelemetry-extension-kotlin:$openTelemetryKotlinVersion")
}
