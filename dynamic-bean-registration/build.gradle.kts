plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    implementation("jakarta.enterprise:jakarta.enterprise.cdi-api")
}
