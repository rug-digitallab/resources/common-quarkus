package nl.rug.digitallab.common.quarkus.helper

import kotlin.reflect.KClass

/**
 * Annotation to indicate that a bean should only be registered when the
 * specified class is present in the classpath.
 *
 * This is useful to provide dynamic behaviour conditional on the
 * classpath. For example, some beans can provide integrations with
 * a third-party library that might not be required for normal
 * operations.
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Repeatable
annotation class IfClass(
    val klass: KClass<*>
)
