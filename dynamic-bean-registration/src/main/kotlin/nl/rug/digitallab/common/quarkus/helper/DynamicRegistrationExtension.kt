package nl.rug.digitallab.common.quarkus.helper

import io.quarkus.arc.VetoedProducer
import jakarta.enterprise.inject.Vetoed
import jakarta.enterprise.inject.build.compatible.spi.*

/**
 * Jakarta CDI build-compatible extension to handle dynamic beans that
 * are conditional on other types and methods being present in the classpath.
 *
 * Build-compatible extensions are a new feature in CDI Lite, triggered
 * by the development of Quarkus. They are the only types of CDI
 * extensions that are supported by Quarkus - unlike portable extensions.
 *
 * This particular extension will iterate over all producers annotated
 * with [IfClass] or [IfMethod], and if the target type or method of the
 * annotation is not in the current classpath, the bean is vetoed.
 */
class DynamicRegistrationExtension : BuildCompatibleExtension {
    /**
     * Callback for all classes annotated with the [IfClass] or [IfMethod]
     * annotation, will check whether the targeted type or method is in the
     * classpath, and when it is not, it will veto the bean type.
     */
    @Enhancement(types = [Any::class], withSubtypes = true, withAnnotations = [IfClass::class, IfMethod::class])
    fun enhanceClasses(bean: ClassConfig, types: Types) = checkAnnotations(bean, types, Vetoed::class.java)

    /**
     * Callback for all methods annotated with the [IfClass] annotation,
     * will check whether the targeted type is in the classpath, and when
     * it is not, it will veto the producer method.
     */
    @Enhancement(types = [Any::class], withSubtypes = true, withAnnotations = [IfClass::class, IfMethod::class])
    fun enhanceMethods(method: MethodConfig, types: Types) = checkAnnotations(method, types, VetoedProducer::class.java)

    /**
     * Function implementing the logic for the [IfClass] and [IfMethod]
     * annotations. Will iterate all such annotations on the given declaration,
     * and will add the specified veto annotation to the bean declaration
     * when the type or method are not present in the classpath.
     *
     * @param declaration The bean declaration to check
     * @param types Build index metadata
     * @param vetoAnnotation The annotation to veto the bean declaration
     */
    private fun checkAnnotations(declaration: DeclarationConfig, types: Types, vetoAnnotation: Class<out Annotation>) {
        // Find the relevant annotations
        val classAnnotations = declaration.info().repeatableAnnotation(IfClass::class.java)
        val methodAnnotations = declaration.info().repeatableAnnotation(IfMethod::class.java)

        classAnnotations.forEach { annotation ->
            // Find the target type. If the build index metadata (in types)
            // cannot be found, that means the type is not available on the
            // classpath, so we should veto the bean.
            val klass = annotation.member("klass")
            if (types.ofClass(klass.toString()) == null) {
                declaration.addAnnotation(vetoAnnotation)
            }
        }

        methodAnnotations.forEach { annotation ->
            // Find the target type and method. If the build index metadata
            // (in types) cannot be found, or does not contain the target
            // method, that means the method is not available on the classpath,
            // so we should veto the bean.
            val klass = annotation.member("klass")
            val method = annotation.member("method").asString()

            val type = types.ofClass(klass.toString())
            if(type == null || type.declaration().methods().none { it.name() == method }) {
                declaration.addAnnotation(vetoAnnotation)
            }
        }
    }
}
