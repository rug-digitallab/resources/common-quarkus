package nl.rug.digitallab.common.quarkus.helper

import kotlin.reflect.KClass

/**
 * Annotation to indicate that a bean should only be registered when the
 * specified method is present in the classpath.
 *
 * This is useful to provide dynamic behaviour conditional on the
 * classpath. For example, some beans can provide integrations with
 * a third-party library that might not be required for normal
 * operations, or are even dependent on specific versions of such
 * a library that have specific methods.
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Repeatable
annotation class IfMethod(
    val klass: KClass<*>,
    val method: String,
)
