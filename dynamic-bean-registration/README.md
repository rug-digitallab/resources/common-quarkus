# Dynamic Bean Registration

The Dynamic Bean Registration library provides utilities to only conditionally register beans in a CDI context. This library is inspired by similar `@ConditionXXXX` annotations and utilities from Spring.

## Usage

Currently, the library automatically registers itself as a CDI Build-Time Extension. So as soon as it is available at the runtime classpath, the extension will be active.

The library currently offers two annotations for your beans: `@IfClass(Type)` and `@IfMethod(Type, String)`. These repeatable annotations can be placed at beans or producer methods, and take a single class parameter and optionally a method name string. Their effect is that the bean or producer will only be active when the specified types or methods are on the runtime classpath. If the types or methods are not available, the bean or producer will be ignored.

These annotations are mainly useful for libraries providing optional integration features with third-party libraries. These integration features (beans, producers) can then be dynamically made available, based on whether the (correct version of the) third-party library is on the classpath.

Example of a basic usage:
```kotlin
@Singleton
@IfClass(ObjectMapper::class)
@IfMethod(InputTransformer::class, "newFeature")
class SomeIntegration {}
```
