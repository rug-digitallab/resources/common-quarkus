# Hibernate ORM
The Hibernate ORM library provides a set of utilities for working with Hibernate in Quarkus applications.

## Prerequisites

- Quarkus Hibernate ORM
- Quarkus Hibernate ORM Panache

## Usage

### BaseRepository 

`BaseRepository` is an abstract class that provides a set of utility methods for working with Hibernate repositories.
The `BaseRepository` class can be used as follows:

```kotlin
@ApplicationScoped
class MyRepository : BaseRepository<MyEntity, MyIdType>() {
    // this will add all the methods from BaseRepository
}
```
