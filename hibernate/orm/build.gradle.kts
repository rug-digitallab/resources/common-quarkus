plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    api("io.quarkus:quarkus-hibernate-orm-panache-kotlin")

    testImplementation("io.quarkus:quarkus-jdbc-mariadb")
    testImplementation("io.rest-assured:kotlin-extensions")
}
