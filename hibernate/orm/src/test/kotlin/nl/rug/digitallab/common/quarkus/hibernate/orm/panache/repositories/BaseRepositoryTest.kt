package nl.rug.digitallab.common.quarkus.hibernate.orm.panache.repositories

import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.quarkus.utils.TestEntity
import nl.rug.digitallab.common.quarkus.utils.TestException
import nl.rug.digitallab.common.quarkus.utils.TestRepository
import nl.rug.digitallab.common.quarkus.utils.TestUtil
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class BaseRepositoryTest {
    @Inject
    lateinit var testRepository: TestRepository

    @Test
    @TestTransaction
    fun `Creating a new entity should return a non-null result`() {
        assertNotNull(testRepository.create(TestUtil.getRandomTestEntity()))
    }

    @Test
    @TestTransaction
    fun `Creating a new entity should return the entity with an id`() {
        val testEntity = TestUtil.getRandomTestEntity()
        testRepository.create(testEntity)

        val foundEntity = testRepository.findById(testEntity.id)
        testEntity.doCompare(foundEntity!!)
    }

    @Test
    fun `Finding a non-existing entity should throw an exception`() {
        val id = TestUtil.getRandomId()
        assertThrows<TestException> { testRepository.findByIdOrThrow(id) { TestUtil.getTestException(id) } }
    }

    @Test
    @TestTransaction
    fun `Updating an entity should return the updated entity`() {
        val testEntity = TestUtil.getRandomTestEntity()
        val newName = "updated"
        testRepository.create(testEntity)

        testRepository.updateWithOrThrow(
            testEntity.id,
            { TestUtil.getTestException(testEntity.id) },
            { it.name = newName }
        )

        assertEquals(testEntity.name, newName)
    }

    @Test
    @TestTransaction
    fun `Updating a non-existing entity should throw an exception`() {
        val id = TestUtil.getRandomId()
        val exception = TestUtil.getTestException(id)

        assertThrows<TestException> { testRepository.updateWithOrThrow(id, { exception }, { it }) }
    }

    @Test
    @TestTransaction
    fun `Deleting an entity should remove it from the datasource`() {
        val testEntity = TestUtil.getRandomTestEntity()
        testRepository.create(testEntity)
        testRepository.deleteByIdOrThrow(testEntity.id) { TestUtil.getTestException(testEntity.id) }

        assertThrows<TestException> {
            testRepository.findByIdOrThrow(testEntity.id) { TestUtil.getTestException(testEntity.id) }
        }
    }

    @Test
    @TestTransaction
    fun `Deleting a non-existing entity should throw an exception`() {
        val id = TestUtil.getRandomId()
        val exception = TestUtil.getTestException(id)

        assertThrows<Exception> { testRepository.deleteByIdOrThrow(id) { exception } }
    }

    @Test
    @TestTransaction
    fun `Null can be passed as a parameter to a query`() {
        testRepository.find("name = ?1 and age = ?2", null, 0)
    }
}

fun TestEntity.doCompare(other: TestEntity) {
    assertEquals(this.id, other.id)
    assertEquals(this.name, other.name)
    assertEquals(this.age, other.age)
}
