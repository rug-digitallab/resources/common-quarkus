package nl.rug.digitallab.common.quarkus.utils

import kotlin.random.Random

object TestUtil {
    fun getRandomTestEntity(): TestEntity {
        return TestEntity("test", Random.nextInt())
    }

    fun getRandomId(): TestEntityId {
        return TestEntityId.randomUUID()
    }

    fun getTestException(id : TestEntityId): TestException {
        return TestException(id)
    }
}
