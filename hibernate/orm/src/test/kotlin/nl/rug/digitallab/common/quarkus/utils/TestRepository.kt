package nl.rug.digitallab.common.quarkus.utils

import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.common.quarkus.hibernate.orm.panache.repositories.BaseRepository

@ApplicationScoped
class TestRepository : BaseRepository<TestEntity, TestEntityId>()
