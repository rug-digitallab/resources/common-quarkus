package nl.rug.digitallab.common.quarkus.utils

import java.util.UUID

typealias TestEntityId = UUID
