package nl.rug.digitallab.common.quarkus.hibernate.orm.panache.repositories

import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepositoryBase

/**
 * Abstract class of a base repository to use within the Digital Lab quarkus applications.
 *
 * In here we define some methods that we know all of our repos will use, across all of our applications.
 */
abstract class BaseRepository<EntityT : Any, IdT : Any> : PanacheRepositoryBase<EntityT, IdT> {
    /**
     * This method will persist and flush the entity immediately, if you want to persist it later, use [persist].
     *
     * @param e The entity to create, and flush.
     */
    fun create(e: EntityT): Unit = persistAndFlush(e)

    /**
     * Find an entity by its [IdT] or throw a dynamic exception if the entity is not found.
     *
     * @param id The id of the entity.
     * @param exception The exception to be thrown in case the entity is not found.
     *
     * @throws Exception The passed exception.
     */
    fun findByIdOrThrow(
        id: IdT,
        exception: (IdT) -> Exception,
    ): EntityT = findById(id) ?: throw exception(id)

    /**
     * Update an [EntityT] with exception. This method will persist and flush the entity immediately.
     *
     * @param id The id of the entity.
     * @param exception The exception to throw in case the entity is not found.
     * @param updateEntity A method to take the old entity and should return [EntityT]
     */
    fun updateWithOrThrow(
        id: IdT,
        exception: (IdT) -> Exception,
        updateEntity: (e: EntityT) -> Unit,
    ) = findByIdOrThrow(id, exception)
        .also { updateEntity(it) }
        .also { persistAndFlush(it) }

    /**
     * Delete an entity by its [IdT] and throw a dynamic exception.
     *
     * @param id The id of the entity to delete.
     * @param exception The exception to be thrown in case the entity is not found.
     *
     * @throws Exception The passed exception.
     */
    fun deleteByIdOrThrow(
        id: IdT,
        exception: (IdT) -> Exception,
    ) = if (!deleteById(id)) throw exception(id) else Unit
}
