# Hibernate Validator
The Hibernate Validator library provides a set of utilities for performing validations using Hibernate Validator.

## Usage

### Posix Pattern

The `PosixPattern` annotation is used on String properties to ensure that the value of the property is compliant with
the POSIX portable filename character set. The `PosixPattern` annotation can be used as follows:

```kotlin
@Entity
class MyEntity(
    @field:PosixPattern
    val fileName: String,
)
```
