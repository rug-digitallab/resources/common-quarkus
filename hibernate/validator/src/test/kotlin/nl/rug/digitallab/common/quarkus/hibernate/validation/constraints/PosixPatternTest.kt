package nl.rug.digitallab.common.quarkus.hibernate.validation.constraints

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import jakarta.validation.Validator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory

@QuarkusTest
class PosixPatternTest {
    @Inject
    private lateinit var validator: Validator

    @TestFactory
    fun `A @PosixPattern annotated field should be invalid when it contains characters that are not in the POSIX portable filename character set`(): List<DynamicTest> {
        return invalidStrings.map {
            DynamicTest.dynamicTest("A @PosixPattern annotated field containing '$it' should be invalid") {
                val testDTO = PosixPatternTestClass(someString = it)
                val violations = validator.validate(testDTO)

                assertEquals(1, violations.size)
                violations.iterator().next().apply {
                    assertEquals("someString", propertyPath.toString())
                    assertEquals(it, invalidValue)
                    assertEquals("May only contain alphanumeric characters, underscores, dashes, dots and spaces", message)
                }
            }
        }
    }

    @TestFactory
    fun `A @PosixPattern annotated field should be valid when it contains only characters that are in the POSIX portable filename character set`(): List<DynamicTest> {
        return validStrings.map {
            DynamicTest.dynamicTest("A @PosixPattern annotated field containing '$it' should be valid") {
                val testDTO = PosixPatternTestClass(someString = it)
                val violations = validator.validate(testDTO)

                assertEquals(0, violations.size)
            }
        }
    }

    @Test
    fun `A @PosixPattern annotated field should be valid when it is null`() {
        val testDTO = PosixPatternTestClass(someString = null)
        val violations = validator.validate(testDTO)

        assertEquals(0, violations.size)
    }

    private val validStrings = listOf(
        "valid",
        "valid_",
        "valid-",
        "valid.",
        "valid ",
        "valid_-. ",
    )

    private val invalidStrings = listOf(
        "!nv@l!d",
        "invalid!",
        "invalid@",
        "invalid#",
        "invalid$",
        "invalid%",
        "invalid^",
        "invalid&",
        "invalid*",
        "invalid(",
        "invalid)",
        "invalid+",
        "invalid=",
        "invalid{",
        "invalid}",
        "invalid[",
        "invalid]",
        "invalid|",
        "invalid\\",
        "invalid:",
        "invalid;",
        "invalid\"",
        "invalid'",
        "invalid<",
        "invalid>",
        "invalid,",
        "invalid?",
        "invalid/",
        "invalid~",
        "invalid`",
    )
}

class PosixPatternTestClass(
    @field:PosixPattern
    var someString: String?
)

