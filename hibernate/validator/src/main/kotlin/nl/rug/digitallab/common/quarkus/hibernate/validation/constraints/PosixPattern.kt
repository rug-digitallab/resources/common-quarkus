package nl.rug.digitallab.common.quarkus.hibernate.validation.constraints

import jakarta.validation.Constraint
import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext
import jakarta.validation.Payload
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext
import org.hibernate.validator.internal.engine.messageinterpolation.util.InterpolationHelper
import java.util.regex.Pattern
import kotlin.annotation.AnnotationTarget.*
import kotlin.reflect.KClass

/**
 * A constraint annotation that validates a string to only contain alphanumeric characters, underscores, dashes, dots
 * and spaces (POSIX portable filename characters).
 *
 * @property message The message to return when the constraint is violated.
 * @property groups The groups the constraint belongs to.
 * @property payload The payload associated to the constraint.
 */
@Target(FIELD, PROPERTY, VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Constraint(validatedBy = [PosixPatternValidator::class])
annotation class PosixPattern(
    val message: String = "May only contain alphanumeric characters, underscores, dashes, dots and spaces",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = [],
)

/**
 * Constraint validator for [PosixPattern]. The implementation is based directly on the implementation of
 * [org.hibernate.validator.internal.constraintvalidators.bv.PatternValidator].
 */
class PosixPatternValidator : ConstraintValidator<PosixPattern, CharSequence?> {
    private val pattern = Pattern.compile("^[\\w. -]+\$")
    private val escapedPattern = InterpolationHelper.escapeMessageParameter(pattern.pattern())

    override fun isValid(value: CharSequence?, constraintValidatorContext: ConstraintValidatorContext?): Boolean {
        // Null values are considered valid, as they should be validated by @NotNull
        if (value == null)
            return true

        // Add the pattern to the message parameters for interpolation
        if (constraintValidatorContext is HibernateConstraintValidatorContext) {
            constraintValidatorContext
                .unwrap(HibernateConstraintValidatorContext::class.java)
                ?.addMessageParameter("regexp", escapedPattern)
        }

        return pattern.matcher(value).matches()
    }
}
