plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    api("io.quarkus:quarkus-hibernate-validator")
}
