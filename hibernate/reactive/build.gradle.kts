plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    api("io.quarkus:quarkus-hibernate-reactive-panache-kotlin")

    testImplementation("io.quarkus:quarkus-jdbc-mariadb")
    testImplementation("io.quarkus:quarkus-reactive-mysql-client")
    testImplementation("io.quarkus:quarkus-test-hibernate-reactive-panache")
    testImplementation("io.rest-assured:kotlin-extensions")
}
