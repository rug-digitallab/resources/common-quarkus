package nl.rug.digitallab.common.quarkus.hibernate.reactive.panache.repositories

import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.kotlin.PanacheRepositoryBase
import io.smallrye.mutiny.Uni

/**
 * Abstract class of a base repository to use within the Digital Lab quarkus applications.
 *
 * In here we define some methods that we know all of our repos will use, across all of our applications.
 */
abstract class BaseRepository<EntityT : Any, IdT : Any> : PanacheRepositoryBase<EntityT, IdT> {
    /**
     * Create an entity and return a [Uni] of the created entity
     * This method will persist and flush the entity immediately, if you want to persist it later, use [persist].
     *
     * @param e The entity to create, and flush
     *
     */
    @WithSession
    fun create(e: EntityT): Uni<EntityT> = persistAndFlush(e)

    /**
     * Find an entity by its [IdT] or throw a dynamic exception if the entity is not found.
     *
     * @param id The id of the entity
     * @param exception The exception to be thrown in case the entity is not found
     *
     * @throws Exception The passed exception
     */
    @WithSession
    fun findByIdOrThrow(
        id: IdT,
        exception: (IdT) -> Exception,
    ): Uni<EntityT> {
        return findById(id)
            .onItem().ifNull().failWith(exception(id))
    }

    /**
     * Update an [EntityT] with exception. This method will persist and flush the entity immediately.
     *
     * @param id The id of the entity
     * @param exception The exception to throw in case the entity is not found
     * @param updateEntity A method to take the old entity and should return [EntityT]
     *
     * @return A [Uni] of the updated [EntityT]
     */
    @WithSession
    fun updateWithOrThrow(
        id: IdT,
        exception: (IdT) -> Exception,
        updateEntity: (e: EntityT) -> EntityT,
    ): Uni<EntityT> {
        return findByIdOrThrow(id, exception)
            .onItem().transform { updateEntity(it) }
            .chain(this::persistAndFlush)
    }

    /**
     * Delete an entity by its [IdT] and throw a dynamic exception
     *
     * @param id The id of the entity
     * @param exception The exception to be thrown in case the entity is not found
     *
     * @throws Exception The passed exception
     */
    @WithSession
    fun deleteByIdOrThrow(
        id: IdT,
        exception: (IdT) -> Exception,
    ): Uni<Unit> {
        return deleteById(id)
            .onItem().transform { deletionSuccess ->
                if (deletionSuccess) Unit
                else throw exception(id)
            }
    }
}
