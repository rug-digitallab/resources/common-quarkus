package nl.rug.digitallab.common.quarkus.utils

import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.common.quarkus.hibernate.reactive.panache.repositories.BaseRepository

@ApplicationScoped
class TestRepository : BaseRepository<TestEntity, TestEntityId>()
