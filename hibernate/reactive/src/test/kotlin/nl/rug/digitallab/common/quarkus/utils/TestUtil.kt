package nl.rug.digitallab.common.quarkus.utils

object TestUtil {
    fun getRandomTestEntity(): TestEntity {
        return TestEntity("test", 1)
    }

    fun getRandomId(): TestEntityId {
        return TestEntityId.randomUUID()
    }

    fun getTestException(id : TestEntityId): TestException {
        return TestException(id)
    }
}
