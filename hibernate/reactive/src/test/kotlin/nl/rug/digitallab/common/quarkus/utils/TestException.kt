package nl.rug.digitallab.common.quarkus.utils

class TestException(message: String) : Exception(message) {
    constructor(id : TestEntityId) : this("Entity with $id not found.")
}
