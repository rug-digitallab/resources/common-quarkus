package nl.rug.digitallab.common.quarkus.hibernate.reactive.panache.repositories

import io.quarkus.test.hibernate.reactive.panache.TransactionalUniAsserter
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import jakarta.inject.Inject
import nl.rug.digitallab.common.quarkus.utils.TestEntity
import nl.rug.digitallab.common.quarkus.utils.TestRepository
import nl.rug.digitallab.common.quarkus.utils.TestUtil
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class BaseRepositoryTest {
    @Inject
    lateinit var testRepository: TestRepository

    @Test
    @RunOnVertxContext
    fun `Creating a new entity should return a non-null result`(asserter: TransactionalUniAsserter) {
        asserter.assertNotNull {
            testRepository.create(TestUtil.getRandomTestEntity())
        }
    }

    @Test
    @RunOnVertxContext
    fun `Creating a new entity should return the entity with an id`(asserter: TransactionalUniAsserter) {
        val testEntity = TestUtil.getRandomTestEntity()
        asserter.execute<TestEntity> {
            testRepository.create(testEntity)
        }.assertThat({
            testRepository.findById(testEntity.id)
        }) { found ->
            found.doCompare(testEntity)
        }
    }

    @Test
    @RunOnVertxContext
    fun `Finding a non-existing entity should throw an exception`(asserter: TransactionalUniAsserter) {
        val id = TestUtil.getRandomId()
        asserter.assertFailedWith(
            { testRepository.findByIdOrThrow(id) { TestUtil.getTestException(id) } }
        ) { e ->
            assertEquals(e.javaClass, TestUtil.getTestException(id).javaClass)
        }
    }

    @Test
    @RunOnVertxContext
    fun `Updating an entity should return the updated entity`(asserter: TransactionalUniAsserter) {
        val testEntity = TestUtil.getRandomTestEntity()
        val newName = "updated"
        asserter.execute<TestEntity> {
            testRepository.create(testEntity)
        }.assertThat({
            testRepository.updateWithOrThrow(
                testEntity.id,
                { TestUtil.getTestException(testEntity.id) },
                { it.name = newName; it }
            )
        }) { updatedEntity ->
            assertEquals(updatedEntity.name, newName)
        }
    }

    @Test
    @RunOnVertxContext
    fun `Updating a non-existing entity should throw an exception`(asserter: TransactionalUniAsserter) {
        val id = TestUtil.getRandomId()
        val exception = TestUtil.getTestException(id)
        asserter.assertFailedWith(
            { testRepository.updateWithOrThrow(id, { exception }, { it }) }
        ) { e ->
            assertEquals(e.javaClass, exception.javaClass)
        }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting an entity should remove it from the datasource`(asserter: TransactionalUniAsserter) {
        val testEntity = TestUtil.getRandomTestEntity()
        asserter.execute<TestEntity> {
            testRepository.create(testEntity)
        }.assertThat({
            testRepository.deleteByIdOrThrow(
                testEntity.id
            ) { TestUtil.getTestException(testEntity.id) }
        }) {
            assertEquals(it, Unit)
        }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting a non-existing entity should throw an exception`(asserter: TransactionalUniAsserter) {
        val id = TestUtil.getRandomId()
        val exception = TestUtil.getTestException(id)
        asserter.assertFailedWith(
            { testRepository.deleteByIdOrThrow(id) { exception } }
        ) { e ->
            assertEquals(e.javaClass, exception.javaClass)
        }
    }
}

fun TestEntity.doCompare(other: TestEntity) {
    assertEquals(this.id, other.id)
    assertEquals(this.name, other.name)
    assertEquals(this.age, other.age)
}
