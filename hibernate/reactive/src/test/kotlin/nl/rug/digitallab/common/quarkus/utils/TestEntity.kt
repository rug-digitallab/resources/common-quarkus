package nl.rug.digitallab.common.quarkus.utils

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id

@Entity
class TestEntity (
    var name: String,
    var age: Int,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: TestEntityId
}
