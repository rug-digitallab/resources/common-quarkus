package nl.rug.digitallab.common.quarkus.exception.mapper.rest

import io.quarkus.runtime.LaunchMode
import io.quarkus.test.junit.QuarkusTest
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.hamcrest.CoreMatchers.*
import org.jboss.resteasy.reactive.RestResponse.Status
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test


@QuarkusTest
class MappedExceptionTestWithProd {
    companion object {
        @JvmStatic
        @BeforeAll
        fun beforeAll() = LaunchMode.set(LaunchMode.NORMAL)

        @JvmStatic
        @AfterAll
        fun afterAll() = LaunchMode.set(LaunchMode.TEST)
    }

    @Test
    fun `MappedExceptions in NORMAL launch mode should not include a stack trace`() {
        When {
            get("/test-resource/bad-request-mapped-exception-endpoint")
        } Then {
            statusCode(Status.BAD_REQUEST.statusCode)
            body("errorMessage", equalTo("BadRequestMappedException"))
            body("errorId", notNullValue())
            body("stackTrace", nullValue())
        }
    }
}
