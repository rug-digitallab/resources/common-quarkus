package nl.rug.digitallab.common.quarkus.exception.mapper.rest

import io.quarkus.test.junit.QuarkusTest
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.hamcrest.CoreMatchers.*
import org.jboss.resteasy.reactive.RestResponse.Status
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.hamcrest.Matchers.`is` as Is

@QuarkusTest
class MappedExceptionTest {
    data class EndpointTestInput(val endpoint: String, val expectedStatusCode: Status, val expectedErrorMessage: String)

    @TestFactory
    fun `Calling an endpoint that generates an exception should result in a MappedException`(): List<DynamicTest> {
        val inputs = listOf(
            EndpointTestInput("/test-resource/bad-request-mapped-exception-endpoint", Status.BAD_REQUEST, "BadRequestMappedException"),
            EndpointTestInput("/test-resource/not-found-mapped-exception-endpoint", Status.NOT_FOUND, "NotFoundMappedException"),
            EndpointTestInput("/test-resource/not-found-exception", Status.NOT_FOUND, "HTTP 404 Not Found"),
            EndpointTestInput("/test-resource/payment-required-mapped-exception-endpoint", Status.PAYMENT_REQUIRED, "PaymentRequiredMappedException"),
            EndpointTestInput("/test-resource/bad-request-web-application-exception-endpoint", Status.BAD_REQUEST, "HTTP 400 Bad Request"),
            EndpointTestInput("/test-resource/normal-exception-endpoint", Status.INTERNAL_SERVER_ERROR, "An internal server error occurred")
        )

        return inputs.map {
            DynamicTest.dynamicTest("Calling ${it.endpoint} should result in a MappedException") {
                When {
                    get(it.endpoint)
                } Then {
                    statusCode(it.expectedStatusCode.statusCode)
                    body("errorMessage", equalTo(it.expectedErrorMessage))
                    body("errorId", notNullValue())
                    body("stackTrace", notNullValue())
                }
            }
        }
    }

    @Test
    fun `Calling an endpoint that generates a NotSupportedException should result in a MappedException`() {
        Given {
            contentType("application/xml") // This should result in a NotSupportedException, which is mapped to a MappedException
        } When {
            post("/test-resource/post-json-endpoint")
        } Then {
            statusCode(equalTo(Status.UNSUPPORTED_MEDIA_TYPE.statusCode))
            body("errorMessage", containsString("The content-type header value did not match"))
            body("errorId", notNullValue())
            body("stackTrace", notNullValue())
        }
    }

    @TestFactory
    fun `Calling an endpoint that generates a ValidationException should have the appropriate error message`(): List<DynamicTest> {
        val inputs = listOf(
            EndpointTestInput(
                "/test-resource/no-message",
                Status.BAD_REQUEST,
                "There was a validation error",
            ),
            EndpointTestInput(
                "/test-resource/with-message",
                Status.BAD_REQUEST,
                "This validation exception has a message",
            ),
            EndpointTestInput(
                "/test-resource/constraint-violations?numViolations=1",
                Status.BAD_REQUEST,
                "must not be blank"
            ),
            EndpointTestInput(
                "/test-resource/constraint-violations?numViolations=3",
                Status.BAD_REQUEST,
                "must not be blank\nmust not be blank\nmust not be blank"
            ),
        )

        return inputs.map { testInput ->
            DynamicTest.dynamicTest("Calling ${testInput.endpoint} should have the appropriate error message") {
                When {
                    get(testInput.endpoint)
                } Then {
                    statusCode(testInput.expectedStatusCode.statusCode)
                    body("errorMessage", Is(testInput.expectedErrorMessage))
                }
            }
        }
    }

}
