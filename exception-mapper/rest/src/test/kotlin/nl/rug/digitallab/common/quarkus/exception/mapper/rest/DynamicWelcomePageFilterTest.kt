package nl.rug.digitallab.common.quarkus.exception.mapper.rest

import io.quarkus.test.junit.QuarkusTest
import io.restassured.module.kotlin.extensions.Extract
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class DynamicWelcomePageFilterTest {
    @Test
    fun `Calling the root endpoint should redirect and return the welcome page`() {
        val response = When {
            get("/")
        } Then {
            statusCode(StatusCode.OK)
        } Extract {
            body().asString()
        }

        assertEquals("Welcome to the dynamic welcome page", response)
    }
}

/**
 * Quarkus' dynamic welcome page is not available in test mode, so we need to create a mock endpoint for it.
 */
@Path("/")
class MockDynamicWelcomePage {
    @Path("/q/dev-ui/welcome")
    @GET
    fun welcome() = "Welcome to the dynamic welcome page"
}
