package nl.rug.digitallab.common.quarkus.exception.mapper.rest

import jakarta.validation.ConstraintViolationException
import jakarta.validation.Validation
import jakarta.validation.ValidationException
import jakarta.validation.constraints.NotBlank
import jakarta.ws.rs.*
import jakarta.ws.rs.core.Response
import org.jboss.resteasy.reactive.RestQuery
import org.jboss.resteasy.reactive.RestResponse.Status
import java.lang.reflect.InvocationTargetException


@Path("test-resource")
class TestResource {
    private val validator = Validation.buildDefaultValidatorFactory().validator

    class BadRequestMappedException: MappedException("BadRequestMappedException", Status.BAD_REQUEST)
    class NotFoundMappedException: MappedException("NotFoundMappedException", Status.NOT_FOUND)
    class PaymentRequiredMappedException: MappedException("PaymentRequiredMappedException", Status.PAYMENT_REQUIRED)
    class BadRequestWebApplicationException: WebApplicationException(Response.Status.BAD_REQUEST)
    class NormalException: Exception("NormalException")

    @GET
    @Path("bad-request-mapped-exception-endpoint")
    @Produces("application/json")
    fun badRequestMappedExceptionEndpoint(): String {
        throw BadRequestMappedException()
    }

    @GET
    @Path("not-found-mapped-exception-endpoint")
    @Produces("application/json")
    fun notFoundMappedExceptionEndpoint(): String {
        throw NotFoundMappedException()
    }

    @GET
    @Path("payment-required-mapped-exception-endpoint")
    @Produces("application/json")
    fun paymentRequiredMappedExceptionEndpoint(): String {
        throw PaymentRequiredMappedException()
    }

    @GET
    @Path("bad-request-web-application-exception-endpoint")
    @Produces("application/json")
    fun badRequestWebApplicationExceptionEndpoint(): String {
        throw BadRequestWebApplicationException()
    }

    @GET
    @Path("normal-exception-endpoint")
    @Produces("application/json")
    fun normalExceptionEndpoint(): String {
        throw NormalException()
    }

    @POST
    @Path("post-json-endpoint")
    @Consumes("application/json")
    @Produces("application/json")
    fun postJsonEndpoint(): Pair<String, String> = "message" to "Hello world"

    @GET
    @Path("not-found-exception")
    @Produces("application/json")
    fun notFoundExceptionEndpoint(): Response {
        throw NotFoundException()
    }

    @GET
    @Path("no-message")
    @Produces("application/json")
    fun validationExceptionNoMessageEndpoint(): String {
        throw ValidationException()
    }

    @GET
    @Path("with-message")
    @Produces("application/json")
    fun validationExceptionWithMessageEndpoint(): String {
        throw ValidationException("This validation exception has a message")
    }

    @GET
    @Path("constraint-violations")
    @Produces("application/json")
    fun oneConstraintViolationExceptionEndpoint(@RestQuery numViolations: Int): String {
        val dtoWithViolations = ValidationTestDTO(List(numViolations) { "" } )

        val violations = validator.validate(dtoWithViolations)
        throw ValidationException(InvocationTargetException(ConstraintViolationException(violations)))
    }

    data class ValidationTestDTO(
        val strings: List<@NotBlank String>
    )
}
