package nl.rug.digitallab.common.quarkus.exception.mapper.rest

import io.quarkus.arc.profile.IfBuildProfile
import jakarta.ws.rs.container.ContainerRequestContext
import jakarta.ws.rs.container.ContainerResponseContext
import jakarta.ws.rs.container.ContainerResponseFilter
import jakarta.ws.rs.ext.Provider
import org.jboss.resteasy.reactive.RestResponse.StatusCode

/**
 * A RESTEasy response filter that redirects to the dynamic welcome page if the root path is not found.
 * This is the default behaviour in Quarkus, but since we override the NotFoundException mapper, we need to implement
 * it ourselves. This filter is only active in development mode.
 */
@Provider
@IfBuildProfile(anyOf = ["dev", "test"])
class DynamicWelcomePageFilter : ContainerResponseFilter {
    override fun filter(request: ContainerRequestContext, response: ContainerResponseContext) {
        if (request.uriInfo.path == "/" && response.status == StatusCode.NOT_FOUND) {
            // Redirect to dynamic welcome page
            response.status = StatusCode.TEMPORARY_REDIRECT
            response.headers["Location"] = listOf("/q/dev-ui/welcome")
        }
    }
}

