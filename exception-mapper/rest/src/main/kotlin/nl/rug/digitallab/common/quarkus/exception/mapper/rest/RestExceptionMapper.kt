package nl.rug.digitallab.common.quarkus.exception.mapper.rest

import jakarta.enterprise.context.ApplicationScoped
import jakarta.validation.ConstraintViolationException
import jakarta.validation.ValidationException
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.NotSupportedException
import jakarta.ws.rs.WebApplicationException
import org.jboss.resteasy.reactive.RestResponse
import org.jboss.resteasy.reactive.server.ServerExceptionMapper

/**
 * A RESTEasy exception mapper that maps exceptions to a [RestResponse] with an [ErrorResponse] JSON body.
 * The rest exception mapper overrides default mappers for [NotSupportedException] and [NotFoundException].
 * The exception mapper is automatically registered by Quarkus as a RESTEasy exception mapper.
 */
@ApplicationScoped
class RestExceptionMapper : AbstractExceptionMapper() {
    /**
     * Maps a [NotSupportedException] to a [RestResponse] with an [ErrorResponse] JSON body.
     *
     * @param e The [NotSupportedException] to map.
     *
     * @return A [RestResponse] with an [ErrorResponse] as JSON body.
     */
    @ServerExceptionMapper
    fun mapException(e: NotSupportedException): RestResponse<ErrorResponse> {
        // Override the default mapper for NotSupportedException
        return handleException(e, e.message, RestResponse.Status.UNSUPPORTED_MEDIA_TYPE)
    }

    /**
     * Maps a [NotFoundException] to a [RestResponse] with an [ErrorResponse] JSON body.
     *
     * @param e The [NotFoundException] to map.
     *
     * @return A [RestResponse] with an [ErrorResponse] as JSON body.
     */
    @ServerExceptionMapper
    fun mapException(e: NotFoundException): RestResponse<ErrorResponse> {
        // Override the default mapper for NotFoundException
        return handleException(e, e.message, RestResponse.Status.NOT_FOUND)
    }

    /**
     * Maps a [WebApplicationException] to a [RestResponse] with an [ErrorResponse] JSON body.
     *
     * @param e The [WebApplicationException] to map.
     *
     * @return A [RestResponse] with an [ErrorResponse] as JSON body.
     */
    @ServerExceptionMapper
    fun mapException(e: WebApplicationException): RestResponse<ErrorResponse> {
        // Override the default mapper for WebApplicationException
        return handleException(e, e.message, RestResponse.Status.fromStatusCode(e.response.status))
    }

    /**
     * Maps a [ValidationException] to a [RestResponse] with an [ErrorResponse] JSON body.
     *
     * @param e The [ValidationException] to map.
     *
     * @return A [RestResponse] with an [ErrorResponse] as JSON body.
     */
    @ServerExceptionMapper
    fun mapException(e: ValidationException): RestResponse<ErrorResponse> {
        // Sometimes the constraint violation is inside a InvocationTargetException cause
        val errorMessage = (e.cause?.cause as? ConstraintViolationException)
                ?.constraintViolations?.joinToString("\n") { it.message }
                // Otherwise use message in exception
                ?: e.message
                // If that's not there, use a default message
                ?: "There was a validation error"

        return handleException(e, errorMessage, RestResponse.Status.BAD_REQUEST)
    }
}
