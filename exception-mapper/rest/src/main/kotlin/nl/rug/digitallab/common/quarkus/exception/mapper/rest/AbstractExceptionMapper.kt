package nl.rug.digitallab.common.quarkus.exception.mapper.rest

import io.opentelemetry.api.trace.StatusCode
import io.opentelemetry.instrumentation.api.instrumenter.LocalRootSpan
import io.quarkus.runtime.LaunchMode
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import org.jboss.logging.Logger
import org.jboss.resteasy.reactive.RestResponse

/**
 * The abstract exception mapper intercepts selected exceptions thrown by the application and maps them to a
 * [RestResponse] with an [ErrorResponse] (JSON) as body. The goal of this exception mapper is to standardize
 * the error responses of the Digital Lab services. The error response contains an errorId that can be used to trace the
 * error in the logs. The errorId is also added to the OTel span. The error response also contains a stack trace, but
 * only when the application is not running in production mode, to prevent leaking internal information.
 *
 * Classes that extend this class should be annotated with [ApplicationScoped], otherwise Quarkus will not register them
 * as RESTEasy exception mappers.
 */
abstract class AbstractExceptionMapper {
    @Inject
    private lateinit var log: Logger

    /**
     * Handles an exception by logging it, creating an [ErrorResponse] JSON body, and adding the errorId to the OTel span.
     *
     * @param e The exception to handle.
     * @param errorMessage The error message to include in the response to the client, this may differ from the [Exception] message.
     * @param httpStatusCode The HTTP status code to include in the response to the client.
     *
     * @return A [RestResponse] with an [ErrorResponse] (JSON) as body.
     */
    protected fun handleException(e: Exception, errorMessage: String?, httpStatusCode: RestResponse.Status): RestResponse<ErrorResponse> {
        log.error("An exception occurred:${System.lineSeparator()}", e)

        val errorResponse = ErrorResponse(errorMessage ?: "An unknown error occurred")

        // Set the status and errorId in the OTel span
        LocalRootSpan
            .current()
            .setStatus(StatusCode.ERROR)
            .setAttribute("errorId", errorResponse.errorId.toString())

        // If the application is not running in production mode, include the stack trace in the response
        if(LaunchMode.current() != LaunchMode.NORMAL) {
            errorResponse.stackTrace = e.stackTraceToString()
        }

        return RestResponse.status(httpStatusCode, errorResponse)
    }
}
