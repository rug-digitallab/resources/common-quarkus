package nl.rug.digitallab.common.quarkus.exception.mapper.rest

import jakarta.enterprise.context.ApplicationScoped
import org.jboss.resteasy.reactive.RestResponse
import org.jboss.resteasy.reactive.server.ServerExceptionMapper

/**
 * A RESTEasy exception mapper that maps exceptions to a [RestResponse] with an [ErrorResponse] JSON body.
 * The base exception mapper is used to map exceptions that do not have a specific mapper. It maps exceptions to a 500
 * error with a default message to prevent leaking internal information. The base exception mapper also maps MappedExceptions
 * to a specific HTTP status code. The exception mapper is automatically registered by Quarkus as a RESTEasy exception mapper.
 */
@ApplicationScoped
class BaseExceptionMapper: AbstractExceptionMapper() {
    /**
     * Maps a [MappedException] to a [RestResponse] with an [ErrorResponse] JSON body. The [MappedException] contains a
     * specific HTTP status code to map the exception to.
     *
     * @param e The [MappedException] to map.
     *
     * @return A [RestResponse] with an [ErrorResponse] as JSON body.
     */
    @ServerExceptionMapper
    fun mapException(e: MappedException): RestResponse<ErrorResponse> {
        return handleException(e, e.message, e.httpStatusCode)
    }

    /**
     * Maps an [Exception] to a [RestResponse] with an [ErrorResponse] JSON body. General exceptions are mapped to a 500
     * error with a default message.
     *
     * @param e The [Exception] to map.
     *
     * @return A [RestResponse] with an [ErrorResponse] as JSON body.
     */
    @ServerExceptionMapper
    fun mapException(e: Exception): RestResponse<ErrorResponse> {
        // Exceptions without a specific mapper are mapped to a 500 error with a default message to prevent leaking internal information
        return handleException(e, "An internal server error occurred", RestResponse.Status.INTERNAL_SERVER_ERROR)
    }
}
