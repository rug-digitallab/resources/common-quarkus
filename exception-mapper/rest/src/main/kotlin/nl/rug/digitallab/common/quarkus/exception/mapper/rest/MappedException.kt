package nl.rug.digitallab.common.quarkus.exception.mapper.rest

import org.jboss.resteasy.reactive.RestResponse.Status

/**
 * Represents an exception that can be mapped to a specific HTTP status code. These exceptions are automatically mapped
 * by the [AbstractExceptionMapper].
 *
 * @param message The error message.
 * @param httpStatusCode The HTTP status code to map the exception to.
 * @param cause The cause of the exception. Defaults to null.
 */
open class MappedException(message: String, val httpStatusCode: Status, cause: Throwable? = null): Exception(message, cause)
