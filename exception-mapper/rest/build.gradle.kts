import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

fun DependencyHandler.hiddenImplementation(dependencyNotation: Any) {
    compileOnly(dependencyNotation)
    testImplementation(dependencyNotation)
}

plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    val mockitoKotlinVersion: String by project

    api("io.quarkus:quarkus-rest")
    api("io.quarkus:quarkus-rest-jackson")
    api("io.quarkus:quarkus-opentelemetry")

    hiddenImplementation("io.quarkus:quarkus-hibernate-validator")

    testImplementation("io.rest-assured:kotlin-extensions")
    testImplementation("io.rest-assured:rest-assured")
    testImplementation("io.quarkus:quarkus-junit5-mockito")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$mockitoKotlinVersion")
}

/**
 * emit-jvm-type-annotations is necessary for jakarta validation of container elements (via annotation of type arguments)
 */
tasks.withType<KotlinCompile> {
    compilerOptions {
        freeCompilerArgs.add("-Xemit-jvm-type-annotations")
    }
}
