# Exception Mapper REST

The Exception Mapper REST library introduces the concept of `MappedException`s, which are exceptions that are mapped to a
specific HTTP status code. This library also provides a RESTEasy exception mapper that handles the following exceptions:
- `MappedException`
- `Exception`
- Common Jakarta RESTful Web Services exceptions

Every exception handled by the mapper is converted to an `ErrorResponse` JSON object. The `ErrorResponse` contains a
unique error ID, an error message, and optionally a stack trace. The stack trace is only visible when the application is
running in development mode. The error ID is generated as a UUID and is automatically logged in OpenTelemetry to enable 
tracing of the error.

## Prerequisites

- Quarkus RESTEasy
- Quarkus OpenTelemetry
- Quarkus Jackson

## Usage

The `BaseExceptionMapper` and `RestExceptionMapper` are automatically loaded when the library is included. 
You can define your own `MappedException`s as follows:

```kotlin
class MyException : MappedException("BadRequestMappedException", Status.BAD_REQUEST)
```

This exception will be automatically mapped to a `400 Bad Request` response.
