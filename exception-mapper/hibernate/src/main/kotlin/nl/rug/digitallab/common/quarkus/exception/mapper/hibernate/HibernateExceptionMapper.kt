package nl.rug.digitallab.common.quarkus.exception.mapper.hibernate

import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.common.quarkus.exception.mapper.rest.AbstractExceptionMapper
import nl.rug.digitallab.common.quarkus.exception.mapper.rest.ErrorResponse
import org.hibernate.HibernateException
import org.jboss.resteasy.reactive.RestResponse
import org.jboss.resteasy.reactive.server.ServerExceptionMapper

/**
 * A RESTEasy exception mapper that maps exceptions to a [RestResponse] with an [ErrorResponse] JSON body. The hibernate
 * exception mapper is used to map [HibernateException]s. The exception mapper is automatically registered by Quarkus as
 * a RESTEasy exception mapper.
 */
@ApplicationScoped
class HibernateExceptionMapper: AbstractExceptionMapper() {
    /**
     * Maps a [HibernateException] to a [RestResponse] with an [ErrorResponse] JSON body. If the [HibernateException] is a
     * [org.hibernate.exception.ConstraintViolationException] or [jakarta.validation.ConstraintViolationException],
     * it is handled separately.
     *
     * @param e The [HibernateException] to map.
     *
     * @return A [RestResponse] with an [ErrorResponse] as JSON body.
     */
    @ServerExceptionMapper
    fun mapHibernateException(e: HibernateException): RestResponse<ErrorResponse> {
        if(e is org.hibernate.exception.ConstraintViolationException)
            return handleConstraintViolationException(e)

        // Sometimes the ConstraintViolationException is wrapped in a HibernateException
        if(e.cause is jakarta.validation.ConstraintViolationException)
            return handleConstraintViolationException(e.cause as Exception)

        // Sometimes the IllegalArgumentException is wrapped in a CompletionException which is wrapped in a HibernateException
        if(e.cause?.cause is IllegalArgumentException)
            return handleConstraintViolationException(e.cause as Exception)

        return handleException(e, "A database error occurred", RestResponse.Status.INTERNAL_SERVER_ERROR)
    }

    /**
     * Constraint violation exceptions are mapped to a 400 error with a specific message.
     *
     * @param e The [Exception] to map.
     *
     * @return A [RestResponse] with an [ErrorResponse] as JSON body.
     */
    private fun handleConstraintViolationException(e: Exception): RestResponse<ErrorResponse> {
        return handleException(e, "A database constraint was violated", RestResponse.Status.BAD_REQUEST)
    }
}
