package nl.rug.digitallab.common.quarkus.exception.mapper.hibernate

import jakarta.ws.rs.*
import org.hibernate.HibernateException
import java.sql.SQLException
import java.util.concurrent.CompletionException

@Path("test-resource")
class TestResource {
    @GET
    @Path("hibernate-exception-endpoint")
    @Produces("application/json")
    fun badRequestMappedExceptionEndpoint(): String {
        throw HibernateException("HibernateException")
    }

    @GET
    @Path("constraint-violation-exception-endpoint")
    @Produces("application/json")
    fun constraintViolationExceptionEndpoint(): String {
        throw org.hibernate.exception.ConstraintViolationException("ConstraintViolationException", SQLException("SQLException"), "constraintName")
    }

    @GET
    @Path("nested-constraint-violation-exception-endpoint")
    @Produces("application/json")
    fun nestedConstraintViolationExceptionEndpoint(): String {
        throw HibernateException("HibernateException", jakarta.validation.ConstraintViolationException("ConstraintViolationException", emptySet()))
    }

    @GET
    @Path("nested-illegal-argument-exception-endpoint")
    @Produces("application/json")
    fun nestedIllegalArgumentExceptionEnpdoint(): String {
        throw HibernateException("HibernateException", CompletionException("CompletionException", IllegalArgumentException("IllegalArgumentException")))
    }
}
