package nl.rug.digitallab.common.quarkus.exception.mapper.hibernate

import io.quarkus.test.junit.QuarkusTest
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.hamcrest.CoreMatchers.*
import org.jboss.resteasy.reactive.RestResponse.Status
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

@QuarkusTest
class HibernateExceptionMapperTest {
    data class EndpointTestInput(val endpoint: String, val expectedStatusCode: Status, val expectedErrorMessage: String)

    @TestFactory
    fun `Calling an endpoint that generates an exception should result in a MappedException`(): List<DynamicTest> {
        val inputs = listOf(
            EndpointTestInput("/test-resource/hibernate-exception-endpoint", Status.INTERNAL_SERVER_ERROR, "A database error occurred"),
            EndpointTestInput("/test-resource/constraint-violation-exception-endpoint", Status.BAD_REQUEST, "A database constraint was violated"),
            EndpointTestInput("/test-resource/nested-constraint-violation-exception-endpoint", Status.BAD_REQUEST, "A database constraint was violated"),
            EndpointTestInput("/test-resource/nested-illegal-argument-exception-endpoint", Status.BAD_REQUEST, "A database constraint was violated"),
        )

        return inputs.map {
            DynamicTest.dynamicTest("Calling ${it.endpoint} should result in a MappedException") {
                When {
                    get(it.endpoint)
                } Then {
                    statusCode(it.expectedStatusCode.statusCode)
                    body("errorMessage", equalTo(it.expectedErrorMessage))
                    body("errorId", notNullValue())
                    body("stackTrace", notNullValue())
                }
            }
        }
    }
}
