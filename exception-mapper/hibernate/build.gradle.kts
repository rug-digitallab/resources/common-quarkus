plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    val mockitoKotlinVersion: String by project

    implementation("io.quarkus:quarkus-hibernate-orm") // HibernateException
    api("io.quarkus:quarkus-hibernate-validator")
    api(project(":exception-mapper:exception-mapper-rest"))

    testImplementation("io.rest-assured:kotlin-extensions")
    testImplementation("io.rest-assured:rest-assured")
    testImplementation("io.quarkus:quarkus-junit5-mockito")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$mockitoKotlinVersion")
}
