# Exception Mapper Hibernate

The Exception Mapper Hibernate library adds a default RESTEasy exception mapper for Hibernate exceptions. For more
information see [Exception Mapper Rest](../rest/README.md).

## Prerequisites

- Quarkus Hibernate
- Exception Mapper REST

## Usage

The `HibernateExceptionMapper` is automatically loaded when the library is included. 
