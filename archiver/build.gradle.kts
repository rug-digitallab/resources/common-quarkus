plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    val commonsCompressVersion: String by project
    val guavaVersion: String by project

    api("io.quarkus:quarkus-opentelemetry")
    api("com.google.guava:guava:$guavaVersion")
    api("org.apache.commons:commons-compress:$commonsCompressVersion")
}
