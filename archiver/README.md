# Archiver

The Archiver library exposes an `ArchiveManager` for archiving and extracting files. Currently, the following archive
types are supported:
* Zip
* Tar

## Usage

Either inject the `ArchiveManager` which will determine the archive type based on the file extension, or use a specific
implementation directly. The `ArchiveManager` is used as follows:

```kotlin
@Inject
lateinit var archiveManager: ArchiveManager

fun example() {
    ...
    archiveManager.archive(sourceDirectory, archiveFile)
    archiveManager.unarchive(archiveFile, destinationDirectory)
}
```

A specific implementation is used as follows:

```kotlin
@Inject
lateinit var zipArchiver: ZipArchiver

fun example() {
    ...
    zipArchiver.archive(sourceDirectory, archiveFile)
    zipArchiver.unarchive(archiveFile, destinationDirectory)
}
```
