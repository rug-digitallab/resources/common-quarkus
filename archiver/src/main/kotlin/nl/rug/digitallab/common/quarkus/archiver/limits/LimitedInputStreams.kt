package nl.rug.digitallab.common.quarkus.archiver.limits

import org.apache.commons.compress.archivers.ArchiveEntry
import org.apache.commons.compress.archivers.ArchiveInputStream
import org.apache.commons.compress.utils.InputStreamStatistics
import java.io.InputStream

/**
 * Wrap the given [InputStream] to provide size limits enforcement.
 * When the given size limits are exceeded, any further operation on the
 * stream will halt. The [InputStream] should implement [InputStreamStatistics].
 *
 * @param limits The limits to enforce
 * @param StreamT The [InputStream] instance to protect with the given limits
 */
fun <StreamT> StreamT.protect(limits: ArchiverLimits): InputStream
    where StreamT : InputStream, StreamT : InputStreamStatistics
{
    return ThresholdingInputStream(this, limits)
}

/**
 * Wrap the given [ArchiveInputStream] to provide archive limits enforcement.
 * When the given limits are exceeded, any further operation on the stream
 * will halt.
 *
 * Note that size limits are only enforced for compressed archive formats such
 * as zip and 7z.
 *
 * @param limits The limits to enforce
 */
fun <T : ArchiveEntry?> ArchiveInputStream<T>.protect(limits: ArchiverLimits): ArchiveInputStream<T> {
    return when (this) {
        is InputStreamStatistics -> ThresholdingStatisticsArchiveInputStream(this, limits)
        else -> ThresholdingArchiveInputStream(this, limits)
    }
}
