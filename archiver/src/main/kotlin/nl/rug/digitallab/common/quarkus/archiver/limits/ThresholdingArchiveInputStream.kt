package nl.rug.digitallab.common.quarkus.archiver.limits

import org.apache.commons.compress.archivers.ArchiveEntry
import org.apache.commons.compress.archivers.ArchiveInputStream

/**
 * Implementation of an [ArchiveInputStream] that wraps an existing
 * instance by delegation, while providing file count limits
 * enforcement through the [ArchiveInputStreamLimitEnforcer]. This
 * implementation does not provide size checks, since not all
 * [ArchiveInputStream]s actually decompress any data. For archive
 * formats that do compress their data, such as zip and 7z, refer
 * to [ThresholdingStatisticsArchiveInputStream].
 *
 * Instantiating this class is best done through the extension function
 * [ArchiveInputStream.protect].
 */
class ThresholdingArchiveInputStream<EntryT : ArchiveEntry?> internal constructor (
    private val innerStream: ArchiveInputStream<EntryT>,
    override val limits: ArchiverLimits,
) : ArchiveInputStream<EntryT>(innerStream, innerStream.charset.name()),
    ArchiveInputStreamLimitEnforcer
{
    override var entryCount = 0L
    override fun getNextEntry(): EntryT {
        val entry = innerStream.nextEntry
        if(entry != null) entryCount++

        checkLimits()

        return entry
    }
}
