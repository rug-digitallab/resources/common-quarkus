package nl.rug.digitallab.common.quarkus.archiver.limits

import org.apache.commons.compress.utils.InputStreamStatistics
import java.io.InputStream

/**
 * Implementation of a [InputStream] that wraps an existing
 * instance by delegation, while providing size limits enforcement
 * through the [InputStreamLimitEnforcer]. This implementation
 * does not provide file count checks, since [InputStream]
 * treat their content as amorphous blobs. For archive formats that
 * compress their data (in addition to packing it), such as zip and 7z,
 * refer to [ThresholdingStatisticsArchiveInputStream].
 *
 * Note that the wrapped [InputStream] instance needs to
 * implement [InputStreamStatistics] - most instances do.
 *
 * Instantiating this class is best done through the extension function
 * [InputStream.protect].
 */
class ThresholdingInputStream<StreamT> internal constructor (
    override val innerStream: StreamT,
    override val limits: ArchiverLimits,
) : InputStream(),
    InputStreamStatistics,
    InputStreamLimitEnforcer
    where StreamT : InputStream, StreamT : InputStreamStatistics
{
    // Unfortunately, [InputStream] does not provide a delegating
    // constructor. Therefore, we have to manually delegate more functions
    // than would otherwise be needed.
    override fun read(): Int = innerStream.read().also { checkLimits() }
    override fun read(b: ByteArray): Int = innerStream.read(b).also { checkLimits() }
    override fun read(b: ByteArray, off: Int, len: Int): Int = innerStream.read(b, off, len).also { checkLimits() }
    override fun skip(n: Long) = innerStream.skip(n)
    override fun available() = innerStream.available()
    override fun close() = innerStream.close()
    override fun mark(readlimit: Int) = innerStream.mark(readlimit)
    override fun reset() = innerStream.reset()
    override fun markSupported() = innerStream.markSupported()

    override fun getCompressedCount() = innerStream.compressedCount
    override fun getUncompressedCount() = (innerStream as InputStreamStatistics).uncompressedCount
}
