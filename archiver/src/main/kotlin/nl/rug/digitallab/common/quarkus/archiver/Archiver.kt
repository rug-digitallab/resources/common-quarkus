package nl.rug.digitallab.common.quarkus.archiver

import com.google.common.net.MediaType
import jakarta.inject.Inject
import nl.rug.digitallab.common.quarkus.archiver.limits.ArchiverLimits
import java.nio.file.Path

/**
 * Abstract class for archiving and unarchiving files.
 *
 * @property supportedMimeTypes The MIME types supported by this archive.
 */
abstract class Archiver {
    @Inject
    protected lateinit var limits: ArchiverLimits

    abstract val supportedMimeTypes: List<MediaType>

    /**
     * Archive a directory to an archive file.
     *
     * @param sourceDirectory The directory to archive.
     * @param destinationArchive The archive file to create.
     *
     * @throws java.io.IOException If an I/O error occurs.
     */
    abstract fun archive(sourceDirectory: Path, destinationArchive: Path)

    /**
     * Unarchive an archive file to a directory.
     *
     * @param sourceArchive The archive file to unarchive.
     * @param destinationDirectory The directory to unarchive to.
     *
     * @throws java.io.IOException If an I/O error occurs.
     */
    abstract fun unarchive(sourceArchive: Path, destinationDirectory: Path)

    fun supports(mimeType: MediaType): Boolean = supportedMimeTypes.contains(mimeType)
}
