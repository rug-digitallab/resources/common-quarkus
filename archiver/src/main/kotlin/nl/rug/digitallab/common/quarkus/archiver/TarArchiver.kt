package nl.rug.digitallab.common.quarkus.archiver

import com.google.common.net.MediaType
import jakarta.inject.Singleton
import nl.rug.digitallab.common.quarkus.archiver.limits.protect
import nl.rug.digitallab.common.quarkus.archiver.util.toOctalFileMode
import org.apache.commons.compress.archivers.tar.TarArchiveEntry
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Path
import nl.rug.digitallab.common.quarkus.archiver.util.toPosixFilePermissions
import kotlin.io.path.getPosixFilePermissions

/**
 * Archive and unarchive TAR files.
 */
@Singleton
class TarArchiver: ApacheArchiver<TarArchiveEntry>() {
    override val supportedMimeTypes: List<MediaType> = listOf(MediaType.TAR)

    override fun createArchiveEntryFromFile(file: File, fileName: String): TarArchiveEntry {
        val entry = TarArchiveEntry(file, fileName)

        tryPosixPermissions {
            val octalMode = file.toPath().getPosixFilePermissions().toOctalFileMode()
            entry.mode = Integer.parseInt(octalMode, 8)
            log.debug("Setting file permissions for $fileName to ${entry.mode.toString(8)}")
        }

        return entry
    }

    override fun createArchiveInputStream(inputStream: InputStream) = TarArchiveInputStream(inputStream).protect(limits)

    override fun createArchiveOutputStream(outputStream: OutputStream) = TarArchiveOutputStream(outputStream)

    override fun onCreateFile(entry: TarArchiveEntry, outputPath: Path) {
        tryPosixPermissions {
            val octalMode = entry.mode.toString(8)
            log.debug("Setting file permissions for $outputPath to $octalMode")
            Files.setPosixFilePermissions(outputPath, octalMode.toPosixFilePermissions())
        }
    }
}
