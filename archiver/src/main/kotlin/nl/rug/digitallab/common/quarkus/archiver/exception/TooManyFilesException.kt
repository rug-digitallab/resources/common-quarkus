package nl.rug.digitallab.common.quarkus.archiver.exception

class TooManyFilesException(limit: Long)
    : LimitsException("Exceeded archive file count limit of $limit")
