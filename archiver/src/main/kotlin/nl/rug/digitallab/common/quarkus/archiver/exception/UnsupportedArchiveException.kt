package nl.rug.digitallab.common.quarkus.archiver.exception

import com.google.common.net.MediaType

class UnsupportedArchiveException(mimeType: MediaType): Exception("Unsupported archive type: $mimeType")