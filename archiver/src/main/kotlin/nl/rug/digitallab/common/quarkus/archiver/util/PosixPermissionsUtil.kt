package nl.rug.digitallab.common.quarkus.archiver.util

import java.nio.file.attribute.PosixFilePermission

/**
 * Constants with respect to file permissions
 * The [OWNER | GROUP | OTHERS]_[READ | WRITE | EXEC]_BIT defines the bit position to which a 1 indicates that
 * permission is set.
 */
internal const val READ_BIT: Int = 4
internal const val WRITE_BIT: Int = 2
internal const val EXEC_BIT: Int = 1

internal const val OWNER_READ_BIT: Int = READ_BIT shl 6
internal const val OWNER_WRITE_BIT: Int = WRITE_BIT shl 6
internal const val OWNER_EXEC_BIT: Int = EXEC_BIT shl 6

internal const val GROUP_READ_BIT: Int = READ_BIT shl 3
internal const val GROUP_WRITE_BIT: Int = WRITE_BIT shl 3
internal const val GROUP_EXEC_BIT: Int = EXEC_BIT shl 3

internal const val OTHERS_READ_BIT: Int = READ_BIT shl 0
internal const val OTHERS_WRITE_BIT: Int = WRITE_BIT shl 0
internal const val OTHERS_EXEC_BIT: Int = EXEC_BIT shl 0

/**
 * Takes [this] octal string representing a set of unix access permission and converts it to the
 * corresponding [PosixFilePermission].
 *
 * @return The set of [PosixFilePermission] corresponding to the octal string.
 */
internal fun String.toPosixFilePermissions(): Set<PosixFilePermission> =
    /*
     * Generate an infinite sequence of powers of 2, so 1, 10, 100, .... These correspond to single permissions
     * with values greater than 1000000 corresponding to no permission.
     */
    generateSequence(1) { it shl 1 }
        // Takes those that less than 100000000, i.e: those corresponding to a single permission
        .takeWhile { it < 1 shl 9 }
        // Take the set of sequences that contain a 1 in the same bit position as the integer this function is executed on
        .filter { (Integer.parseInt(this, 8) and it) != 0 }
        // Map the bit sequence to the specific [PosixFilePermission]
        .map {
            when (it) {
                OTHERS_READ_BIT -> PosixFilePermission.OTHERS_READ
                OTHERS_WRITE_BIT -> PosixFilePermission.OTHERS_WRITE
                OTHERS_EXEC_BIT -> PosixFilePermission.OTHERS_EXECUTE

                GROUP_READ_BIT -> PosixFilePermission.GROUP_READ
                GROUP_WRITE_BIT -> PosixFilePermission.GROUP_WRITE
                GROUP_EXEC_BIT -> PosixFilePermission.GROUP_EXECUTE

                OWNER_READ_BIT -> PosixFilePermission.OWNER_READ
                OWNER_WRITE_BIT -> PosixFilePermission.OWNER_WRITE
                OWNER_EXEC_BIT -> PosixFilePermission.OWNER_EXECUTE

                else -> error("Unknown bit flag")
            }
        }
        .toSet()

/**
 * Takes [this] set of permissions and returns the corresponding octal string, for unix file access permissions.
 *
 * @return The octal string representing the set of permissions.
 */
internal fun Set<PosixFilePermission>.toOctalFileMode() = sumOf { permission ->
    /*
     * For each permission in the set, find the corresponding bit sequence. Then we add all the bit sequences, resulting
     * in an integer where if a bit position is set to 1, then there was a bit sequence that contained a 1 at that
     * bit position. This is because none of the bit positions overlap.
     */
    when (permission) {
        PosixFilePermission.OTHERS_READ -> OTHERS_READ_BIT
        PosixFilePermission.OTHERS_WRITE -> OTHERS_WRITE_BIT
        PosixFilePermission.OTHERS_EXECUTE -> OTHERS_EXEC_BIT

        PosixFilePermission.GROUP_READ -> GROUP_READ_BIT
        PosixFilePermission.GROUP_WRITE -> GROUP_WRITE_BIT
        PosixFilePermission.GROUP_EXECUTE -> GROUP_EXEC_BIT

        PosixFilePermission.OWNER_READ -> OWNER_READ_BIT
        PosixFilePermission.OWNER_WRITE -> OWNER_WRITE_BIT
        PosixFilePermission.OWNER_EXECUTE -> OWNER_EXEC_BIT
    }
}.toString(8)
