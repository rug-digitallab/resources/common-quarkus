package nl.rug.digitallab.common.quarkus.archiver.exception

class ArchiveInflationException(ratioLimit: Long) :
    LimitsException("Exceeded archive entry inflation limit of ${ratioLimit}x")
