package nl.rug.digitallab.common.quarkus.archiver.limits

import org.apache.commons.compress.archivers.ArchiveEntry
import org.apache.commons.compress.archivers.ArchiveInputStream
import org.apache.commons.compress.utils.InputStreamStatistics

/**
 * Implementation of a compressing [ArchiveInputStream] that wraps
 * an existing instance by delegation, while providing file count and
 * size limits enforcement through [ArchiveInputStreamLimitEnforcer]
 * and [InputStreamLimitEnforcer].
 *
 * This implementation is different from [ThresholdingArchiveInputStream]
 * since this also supports decompression limits enforcement - specific
 * to archive formats that compress their data, such as zip and 7z. Therefore,
 * the wrapped [ArchiveInputStream] instance needs to implement
 * [InputStreamStatistics] - which zip and 7z do.
 *
 * Instantiating this class is best done through the extension function
 * [ArchiveInputStream.protect], which will automatically use this implementation
 * when the [InputStreamStatistics] interface is available.
 */
class ThresholdingStatisticsArchiveInputStream<EntryT : ArchiveEntry?, StreamT> internal constructor (
    override val innerStream: StreamT,
    override val limits: ArchiverLimits,
) : ArchiveInputStream<EntryT>(innerStream, innerStream.charset.name()),
    InputStreamStatistics,
    ArchiveInputStreamLimitEnforcer,
    InputStreamLimitEnforcer
    where StreamT : ArchiveInputStream<EntryT>, StreamT : InputStreamStatistics
{
    // Since [ArchiveInputStream] provides a delegating constructor, we can just
    // call super for these overrides instead of delegating to [innerStream] ourselves.
    override fun read(b: ByteArray): Int = super.read(b).also { checkLimits() }
    override fun read(b: ByteArray, off: Int, len: Int): Int = super.read(b, off, len).also { checkLimits() }
    override fun skip(n: Long): Long = super.skip(n).also { checkLimits() }

    override fun getCompressedCount() = innerStream.compressedCount
    override fun getUncompressedCount() = innerStream.uncompressedCount

    override var entryCount = 0L
    override fun getNextEntry(): EntryT {
        val entry = innerStream.nextEntry
        if(entry != null) entryCount++

        checkLimits()

        return entry
    }

    // Since we override both limit enforcer implementations, we need
    // to explicitly call both check implementations to resolve the
    // multiple inheritance conflict.
    override fun checkLimits() {
        super<ArchiveInputStreamLimitEnforcer>.checkLimits()
        super<InputStreamLimitEnforcer>.checkLimits()
    }
}
