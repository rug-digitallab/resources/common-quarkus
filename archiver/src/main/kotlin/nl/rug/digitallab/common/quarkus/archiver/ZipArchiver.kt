package nl.rug.digitallab.common.quarkus.archiver

import com.google.common.net.MediaType
import com.kamelia.sprinkler.util.closeableScope
import jakarta.inject.Singleton
import nl.rug.digitallab.common.quarkus.archiver.exception.TooManyFilesException
import nl.rug.digitallab.common.quarkus.archiver.limits.protect
import nl.rug.digitallab.common.quarkus.archiver.util.toOctalFileMode
import nl.rug.digitallab.common.quarkus.archiver.util.toPosixFilePermissions
import org.apache.commons.compress.archivers.ArchiveInputStream
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream
import org.apache.commons.compress.archivers.zip.ZipFile
import org.apache.commons.compress.utils.InputStreamStatistics
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.getPosixFilePermissions
import kotlin.io.path.outputStream

/**
 * Archive and unarchive ZIP files.
 *
 * ZipArchiveInputStream does not support permissions, so be wary of calls to [createArchiveInputStream]. Calling
 * [unarchive] will use [ZipFile] instead of [ZipArchiveInputStream] to enable support for unix permissions.
 */
@Singleton
class ZipArchiver : ApacheArchiver<ZipArchiveEntry>() {
    override val supportedMimeTypes: List<MediaType> = listOf(MediaType.ZIP)

    override fun createArchiveEntryFromFile(file: File, fileName: String): ZipArchiveEntry {
        val entry = ZipArchiveEntry(file, fileName)

        tryPosixPermissions {
            val octalMode = file.toPath().getPosixFilePermissions().toOctalFileMode()
            entry.unixMode = Integer.parseInt(octalMode, 8)
        }

        return entry
    }

    override fun unarchive(sourceArchive: Path, destinationDirectory: Path) {
        // We override the default unarchive behaviour that depends on [ZipArchiveInputStream] and
        // replace it with [ZipFile]. This is because [ZipArchiveInputStream] does not support unix permissions.

        val zipFileBuilder = ZipFile
            .builder()
            .setPath(sourceArchive)

        zipFileBuilder.get().use { zipFile ->
            var entryCount = 0L

            zipFile.entries.iterator().forEach { entry ->
                if(entryCount > limits.maxFileCount)
                    throw TooManyFilesException(limits.maxFileCount)

                entryCount++

                // Check if entry can be read (archive is supported)
                require(zipFile.canReadEntryData(entry)) {
                    "Cannot read archive entry data: ${entry.name}"
                }

                unarchiveEntry(entry, destinationDirectory) { outputFile ->
                    closeableScope {
                        val inputStream = using(zipFile.getInputStream(entry))

                        // This also smart casts to InputStreamStatistics, allowing us to use protect(limits).
                        require(inputStream is InputStreamStatistics) {
                            "The input stream must implement InputStreamStatistics"
                        }

                        val outputStream = using(outputFile.outputStream())
                        inputStream.protect(limits).copyTo(outputStream)
                    }
                }
            }
        }
    }

    override fun createArchiveInputStream(inputStream: InputStream) = (ZipArchiveInputStream(inputStream) as ArchiveInputStream<ZipArchiveEntry>).protect(limits)

    override fun createArchiveOutputStream(outputStream: OutputStream) = ZipArchiveOutputStream(outputStream)

    override fun onCreateFile(entry: ZipArchiveEntry, outputPath: Path) {
        // Not all zip implementations support permissions, so we need to check if the entry has a unix mode.
        if(entry.unixMode == 0)
            return

        tryPosixPermissions {
            val octalMode = entry.unixMode.toString(8)
            log.debug("Setting file permissions for $outputPath to $octalMode")
            Files.setPosixFilePermissions(outputPath, octalMode.toPosixFilePermissions())
        }
    }

}
