package nl.rug.digitallab.common.quarkus.archiver.exception

import java.nio.file.Path

class UnknownMimeTypeException(file: Path): Exception("Unknown MIME type for file '$file'")