package nl.rug.digitallab.common.quarkus.archiver.exception

import java.lang.Exception

/**
 * Generic exception to report any scenario in which archival limits
 * were exceeded.
 *
 * @param message The message describing the exception
 */
abstract class LimitsException(message: String) : Exception(message)
