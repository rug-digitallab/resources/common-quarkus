package nl.rug.digitallab.common.quarkus.archiver

import com.kamelia.sprinkler.util.closeableScope
import jakarta.inject.Inject
import org.apache.commons.compress.archivers.ArchiveEntry
import org.apache.commons.compress.archivers.ArchiveInputStream
import org.apache.commons.compress.archivers.ArchiveOutputStream
import org.jboss.logging.Logger
import java.io.*
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.*

/**
 * Abstract class for archiving and unarchiving files using Apache Commons Compress.
 *
 * All Apache Commons Compress archive formats, except for 7z, use the same API.
 */
abstract class ApacheArchiver<T : ArchiveEntry> : Archiver() {
    @Inject
    protected lateinit var log: Logger

    /**
     * Archive an input stream to an archive output stream using an archive entry.
     *
     * @param sourceStream The input stream to archive.
     * @param destinationStream The archive output stream to write to.
     * @param entry The archive entry to write to the archive.
     *
     * @throws IOException If an I/O error occurs.
     */
    fun archive(sourceStream: InputStream, destinationStream: ArchiveOutputStream<T>, entry: T) {
        destinationStream.putArchiveEntry(entry)

        // If the path is a file, we copy the contents to the archive entry
        if (!entry.isDirectory)
            sourceStream.copyTo(destinationStream)

        destinationStream.closeArchiveEntry()
    }

    /**
     * Unarchive an archive input stream to an output stream using an archive entry.
     *
     * @param sourceStream The archive input stream to unarchive.
     * @param destinationStream The output stream to write to.
     * @param entry The archive entry to unarchive.
     *
     * @throws IOException If an I/O error occurs.
     */
    fun unarchive(sourceStream: ArchiveInputStream<T>, destinationStream: OutputStream, entry: T) {
        // If the entry is a file, we copy the entry contents to the file
        if (!entry.isDirectory)
            sourceStream.copyTo(destinationStream)
    }

    @OptIn(ExperimentalPathApi::class)
    override fun archive(sourceDirectory: Path, destinationArchive: Path) {
        createArchiveOutputStream(destinationArchive.outputStream()).use { archiveOutputStream ->
            // Archive each file in the directory
            sourceDirectory.walk(PathWalkOption.INCLUDE_DIRECTORIES).forEach { path ->
                val relativePath = sourceDirectory.relativize(path)
                val entry = createArchiveEntryFromFile(path.toFile(), relativePath.toString())

                // Skip the root directory
                if(entry.name == "/" || entry.name == "./")
                    return@forEach

                log.debug("Archiving file ${path.absolute()} to archive entry ${entry.name}")

                if(entry.isDirectory) {
                    // Create an entry for the directory, but don't write any data
                    archiveOutputStream.putArchiveEntry(entry)
                    archiveOutputStream.closeArchiveEntry()
                } else {
                    // Calling path.inputStream() will throw an AccessDeniedException on Windows when the path
                    // is a directory. On other platforms, no exception will be thrown.
                    path.inputStream().use { inputStream ->
                        archive(inputStream, archiveOutputStream, entry)
                    }
                }
            }
        }
    }

    override fun unarchive(sourceArchive: Path, destinationDirectory: Path) {
        val fileInputStream = sourceArchive.inputStream()
        val archiveInputStream = createArchiveInputStream(fileInputStream)

        closeableScope(fileInputStream, archiveInputStream) {
            while (true) {
                val entry = archiveInputStream.nextEntry ?: break

                // Check if entry can be read (archive is supported)
                require(archiveInputStream.canReadEntryData(entry)) {
                    "Cannot read archive entry data: ${entry.name}"
                }

                unarchiveEntry(entry, destinationDirectory) { outputFile ->
                    outputFile.outputStream().use { outputStream ->
                        unarchive(archiveInputStream, outputStream, entry)
                    }
                }
            }
        }
    }

    /**
     * Unarchive an archive entry to a destination directory by writing to an output file. This logic is shared between
     * the [ApacheArchiver] and [ZipArchiver].
     *
     * @param entry The archive entry to unarchive.
     * @param destinationDirectory The destination directory to unarchive to.
     * @param writeToOutputFile The function to write the output file.
     */
    protected fun unarchiveEntry(entry: T, destinationDirectory: Path, writeToOutputFile: (Path) -> Unit) {
        // Skip the root directory, as it is already created
        if(entry.name == "/" || entry.name == "./")
            return

        val outputFile = destinationDirectory.resolve(entry.name)
        log.debug("Unarchiving archive entry: ${entry.name} to ${outputFile.absolute()}")

        // Create the file or directory on the file system
        createFile(entry, outputFile)

        // If the entry is a directory, we don't need to write to the output file
        if (entry.isDirectory)
            return

        writeToOutputFile(outputFile)
    }

    /**
     * Create an [ArchiveEntry] for a file based on the attributes of the input file.
     *
     * @param file The file represented by the [ArchiveEntry].
     * @param fileName The name of the [ArchiveEntry].
     *
     * @return The [ArchiveEntry] for the file.
     */
    abstract fun createArchiveEntryFromFile(file: File, fileName: String): T

    /**
     * Create an [ArchiveOutputStream] from an [OutputStream].
     *
     * @param outputStream The [OutputStream] to create the [ArchiveOutputStream] from.
     *
     * @return The [ArchiveOutputStream] created from the [OutputStream].
     */
    abstract fun createArchiveOutputStream(outputStream: OutputStream): ArchiveOutputStream<T>

    /**
     * Create an [ArchiveInputStream] from an [InputStream].
     *
     * @param inputStream The [InputStream] to create the [ArchiveInputStream] from.
     *
     * @return The [ArchiveInputStream] created from the [InputStream].
     */
    abstract fun createArchiveInputStream(inputStream: InputStream): ArchiveInputStream<T>

    /**
     * Called after a file is created to apply archiver specific operations.
     *
     * @param entry The [ArchiveEntry] to create the file with
     * @param outputPath The path to create the file to
     */
    abstract fun onCreateFile(entry: T, outputPath: Path)

    /**
     * Helper function to wrap setting file mode in a try-catch block.
     *
     * @param posixPermissionsOperation The function to execute.
     */
    protected fun tryPosixPermissions(posixPermissionsOperation: () -> Unit) {
        try {
            // Will only work on systems that are POSIX compliant
            posixPermissionsOperation()
        } catch (e: UnsupportedOperationException) {
            log.warn("PosixFileAttributeView is not supported on this file system: ${e.message}")
        }
    }

    /**
     * Creates a file based on [entry], to [outputPath]
     *
     * @param entry The [ArchiveEntry] to create the file with
     * @param outputPath The path to create the file to
     */
    protected fun createFile(entry: T, outputPath: Path) {
        // Sometimes there is no entry for the parent, so we need to create it
        if(outputPath.parent != null && !outputPath.parent.exists()) {
            log.debug("Creating parent directory: ${outputPath.parent}")
            Files.createDirectories(outputPath.parent)
        }

        // Entry can be either a file or a directory
        if(entry.isDirectory) {
            log.debug("Creating directory: $outputPath")
            Files.createDirectory(outputPath)
        } else {
            log.debug("Creating file: $outputPath")
            Files.createFile(outputPath)
        }

        // Apply archiver specific operations
        onCreateFile(entry, outputPath)
    }
}
