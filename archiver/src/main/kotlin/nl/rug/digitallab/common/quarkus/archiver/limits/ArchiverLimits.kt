package nl.rug.digitallab.common.quarkus.archiver.limits

import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithConverter
import io.smallrye.config.WithDefault
import io.smallrye.config.WithName
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config.ByteSizeLongConverter
import nl.rug.digitallab.common.quarkus.archiver.exception.ArchiveEntryTooLargeException
import nl.rug.digitallab.common.quarkus.archiver.exception.ArchiveInflationException
import nl.rug.digitallab.common.quarkus.archiver.exception.TooManyFilesException

/**
 * Set of limits governing safe unarchiving of untrusted archives. The
 * defaults are based on the Apache POI ZipSecureFile implementation:
 * https://github.com/apache/poi/blob/trunk/poi-ooxml/src/main/java/org/apache/poi/openxml4j/util/ZipSecureFile.java#L41
 */
@ConfigMapping(prefix = "digital-lab.common-quarkus.archiver.limits")
interface ArchiverLimits {
    /**
     * Sets the maximum file count for any archive format. When the unpacked
     * data exceeds this number of files, unarchiving will halt with
     * a [TooManyFilesException].
     *
     * Default is set to 1000 files.
     */
    @get:WithDefault("1000")
    val maxFileCount: Long

    /**
     * Sets the maximum inflated entry size of any compressed archive format.
     * When the uncompressed size of the current entry (file) exceeds this limit,
     * unarchiving will halt with an [ArchiveEntryTooLargeException].
     *
     * Default is set to 1GB.
     */
    @get:WithConverter(ByteSizeLongConverter::class)
    @get:WithName("max-entry-inflated-size")
    @get:WithDefault("1GB")
    val maxEntryInflatedSize: ByteSize

    /**
     * Sets the threshold for the inflation ratio limit. Compressed archive
     * entries that uncompress to less than this many bytes are not subject
     * to the inflation ratio check - to prevent false positives for small
     * files in archives that are no risk to system integrity.
     *
     * When the uncompressed data exceeds this threshold, the check for
     * the [maxEntryInflationRatio] will become active.
     *
     * Default is set to 100KB.
     */
    @get:WithConverter(ByteSizeLongConverter::class)
    @get:WithName("entry-inflation-threshold")
    @get:WithDefault("100KB")
    val entryInflationThreshold: ByteSize

    /**
     * Sets the maximum entry inflation ratio for any compressed archive format.
     * When the uncompressed data of the current entry (file) exceeds this
     * inflation ratio, unarchiving will halt with an [ArchiveInflationException].
     *
     * Default is set to 100x.
     */
    @get:WithDefault("100")
    val maxEntryInflationRatio: Long
}
