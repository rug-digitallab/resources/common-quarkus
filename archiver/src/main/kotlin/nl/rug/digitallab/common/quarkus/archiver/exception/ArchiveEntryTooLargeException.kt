package nl.rug.digitallab.common.quarkus.archiver.exception

import nl.rug.digitallab.common.kotlin.quantities.ByteSize

class ArchiveEntryTooLargeException(sizeLimit: ByteSize) :
    LimitsException("Exceeded archive entry size limit of $sizeLimit")
