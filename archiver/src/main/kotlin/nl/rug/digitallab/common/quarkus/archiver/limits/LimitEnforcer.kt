package nl.rug.digitallab.common.quarkus.archiver.limits

import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.quarkus.archiver.exception.ArchiveInflationException
import nl.rug.digitallab.common.quarkus.archiver.exception.ArchiveEntryTooLargeException
import nl.rug.digitallab.common.quarkus.archiver.exception.TooManyFilesException
import org.apache.commons.compress.utils.InputStreamStatistics

/**
 * Generic interface for a "limit enforcer" on an input stream.
 * We only support two variants of this enforcer: the
 * [ArchiveInputStreamLimitEnforcer] for archive streams, and the
 * [InputStreamLimitEnforcer] for input streams.
 *
 * Examples: jar, tar for the archive streams, and gzip, bzip,
 * lzma, xz, etc for compressed streams.
 * Zip, 7z, and similar are exceptions that are both compressed
 * and archives simultaneously - they need to inherit from both
 * enforcer interfaces.
 *
 * The code implementing these checks is inspired by the ZipSecureFile
 * and ZipArchiveThresholdInputStream from Apache POI:
 * https://github.com/apache/poi/blob/trunk/poi-ooxml/src/main/java/org/apache/poi/openxml4j/util/ZipSecureFile.java
 */
sealed interface LimitEnforcer {
    val limits: ArchiverLimits

    /**
     * Function that will keep track of the current stream state
     * and check the state against the provided limits. When limits
     * are exceeded, this function should throw an applicable exception.
     *
     * Note: This function should be internal or protected, but that
     * is not (yet?) possible on interfaces in Kotlin.
     */
    fun checkLimits()
}

/**
 * Implementation of the archive limits enforcer for [ArchiveInputStream]s.
 * These streams only need the file count check, as typically they are not
 * compressed.
 */
interface ArchiveInputStreamLimitEnforcer : LimitEnforcer {
    /**
     * Users of this interface should use this variable to count the current
     * number of extracted files/entries.
     */
    var entryCount: Long

    override fun checkLimits() {
        if(entryCount > limits.maxFileCount)
            throw TooManyFilesException(limits.maxFileCount)
    }
}

/**
 * Implementation of the archive limits enforcer for [InputStream]s.
 * These streams consider their content to be amorphous compressed blobs, so
 * we check for uncompressed size and the compression ratio, but we cannot
 * check for file count.
 *
 * The statistics we need are only available in the [InputStreamStatistics]
 * interface, which is implemented by essentially all relevant streams.
 */
interface InputStreamLimitEnforcer : LimitEnforcer {
    val innerStream: InputStreamStatistics

    override fun checkLimits() {
        val uncompressedSize = innerStream.uncompressedCount.B

        if(uncompressedSize > limits.maxEntryInflatedSize)
            throw ArchiveEntryTooLargeException(limits.maxEntryInflatedSize)

        // According to the source of ZipArchiveThresholdInputStream, there
        // is a bug in the compressed count that can trigger an NPE somewhere
        // deep down this library. We just ignore this case, and skip the
        // compression ratio check.
        // https://github.com/apache/poi/blob/trunk/poi-ooxml/src/main/java/org/apache/poi/openxml4j/util/ZipArchiveThresholdInputStream.java#L116
        // https://issues.apache.org/jira/browse/COMPRESS-598
        val compressedSize = try {
            innerStream.compressedCount.B
        } catch (e: NullPointerException) {
            return
        }

        // To prevent dividing by 0 - when we have not read anything yet we also
        // have not exceeded the inflation ratio.
        if(compressedSize == 0.B)
            return

        val compressionRatio = uncompressedSize.B / compressedSize.B

        if(uncompressedSize > limits.entryInflationThreshold && compressionRatio > limits.maxEntryInflationRatio)
            throw ArchiveInflationException(limits.maxEntryInflationRatio)
    }
}
