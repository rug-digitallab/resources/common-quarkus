package nl.rug.digitallab.common.quarkus.archiver

import com.kamelia.sprinkler.util.closeableScope
import io.quarkus.arc.All
import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.EphemeralDirectory
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.EphemeralFile
import nl.rug.digitallab.common.quarkus.archiver.util.toOctalFileMode
import nl.rug.digitallab.common.quarkus.archiver.util.toPosixFilePermissions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.condition.DisabledOnOs
import org.junit.jupiter.api.condition.OS
import kotlin.io.path.*

@QuarkusTest
class ApacheArchiverPermissionsTest {
    @All
    lateinit var archivers: MutableList<Archiver>

    @TestFactory
    @DisabledOnOs(OS.WINDOWS)
    fun `File permissions should be preserved when archiving and unarchiving`(): List<DynamicTest> {
        return archivers.map { archiver ->
            DynamicTest.dynamicTest("File permissions should be preserved when archiving and unarchiving with the ${archiver.javaClass.simpleName}") {
                closeableScope {
                    val directoryToArchive = using(EphemeralDirectory()).path

                    // Create a file
                    val aFile = directoryToArchive.resolve("file.txt")
                    aFile.createFile()

                    aFile.setPosixFilePermissions("764".toPosixFilePermissions())
                    aFile.writeText("Hello, world!")

                    assertEquals("764", aFile.getPosixFilePermissions().toOctalFileMode())

                    // Create a directory
                    val aDirectory = directoryToArchive.resolve("directory")
                    aDirectory.createDirectory()
                    aDirectory.setPosixFilePermissions("777".toPosixFilePermissions())

                    assertEquals("777", aDirectory.getPosixFilePermissions().toOctalFileMode())

                    val destinationDirectory = using(EphemeralDirectory()).path
                    val destinationFile = using(EphemeralFile(suffix = ".tar")).path

                    archiver.archive(directoryToArchive, destinationFile)
                    archiver.unarchive(destinationFile, destinationDirectory)

                    assertEquals("764", destinationDirectory.resolve("file.txt").getPosixFilePermissions().toOctalFileMode())
                    assertEquals("777", destinationDirectory.resolve("directory").getPosixFilePermissions().toOctalFileMode())
                }
            }
        }
    }
}
