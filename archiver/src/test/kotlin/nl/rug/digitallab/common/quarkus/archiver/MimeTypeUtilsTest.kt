package nl.rug.digitallab.common.quarkus.archiver

import com.google.common.net.MediaType
import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralFile
import nl.rug.digitallab.common.quarkus.archiver.MimeTypeUtils.getMimeType
import nl.rug.digitallab.common.quarkus.archiver.exception.UnknownMimeTypeException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class MimeTypeUtilsTest {
    @Test
    fun `getMimeType should return correct MIME type for known extension`(): Unit = withEphemeralFile(suffix = ".txt") { tempFile ->
        val expectedMimeType = MediaType.parse("text/plain")
        val mimeType = tempFile.getMimeType()

        assertEquals(expectedMimeType, mimeType)
    }

    @Test
    fun `getMimeType should return default MIME type for unknown extension`(): Unit = withEphemeralFile(suffix = ".xyz") { tempFile ->
        val expectedMimeType = MediaType.OCTET_STREAM
        val mimeType = tempFile.getMimeType()

        assertEquals(expectedMimeType, mimeType)
    }

    @Test
    fun `getMimeType should throw UnknownMimeTypeException for directory`(): Unit = withEphemeralDirectory { tempDirectory ->
        assertThrows<UnknownMimeTypeException> {
            tempDirectory.getMimeType()
        }
    }
}
