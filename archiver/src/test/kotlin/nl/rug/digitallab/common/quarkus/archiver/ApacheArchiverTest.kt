package nl.rug.digitallab.common.quarkus.archiver

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.io.*

@QuarkusTest
class ApacheArchiverTest {
    @Inject
    lateinit var zipArchiver: ZipArchiver

    @Test
    fun `The low-level API should handle archiving and unarchiving on a stream level`() {
        val pipedOutputStream = PipedOutputStream()
        val pipedInputStream = PipedInputStream()
        pipedOutputStream.connect(pipedInputStream)

        // Pipe "test message" into the byte array input stream, which is then piped to the archive output stream
        // to be archived into the zip archive according to the provided entry. The archive output stream is connected
        // to the piped output stream, which forwards the data to the piped input stream.
        val byteArrayInputStream = ByteArrayInputStream("Test message".toByteArray())
        val archiveOutputStream = zipArchiver.createArchiveOutputStream(pipedOutputStream)
        val entry = ZipArchiveEntry("test.txt")
        zipArchiver.archive(byteArrayInputStream, archiveOutputStream, entry)

        // Pipe the data from the piped input stream to the archive input stream, which is then unarchived into the
        // byte array output stream according to the provided entry. The byte array output stream is then read to
        // retrieve the unarchived data.
        val archiveInputStream = zipArchiver.createArchiveInputStream(pipedInputStream)
        val retrievedEntry = archiveInputStream.nextEntry
        val byteArrayOutputStream = ByteArrayOutputStream()
        zipArchiver.unarchive(archiveInputStream, byteArrayOutputStream, retrievedEntry)

        assertEquals(entry.name, retrievedEntry.name)
        assertEquals("Test message", String(byteArrayOutputStream.toByteArray()))
    }
}
