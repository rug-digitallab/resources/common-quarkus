package nl.rug.digitallab.common.quarkus.archiver

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class ZipArchiverTest {
    @Inject
    lateinit var zipArchiver: ZipArchiver

    @Test
    fun `The unarchive() function should throw an exception when the ZIP is encrypted and not empty`(): Unit =
        withEphemeralDirectory(prefix = "dest") { destinationDirectory ->
            val archiveFile = getResource("file/encryption/encrypted.zip").asPath()

            assertThrows<IllegalArgumentException> {
                zipArchiver.unarchive(archiveFile, destinationDirectory)
            }
        }
}
