package nl.rug.digitallab.common.quarkus.archiver

import com.google.common.net.MediaType
import com.kamelia.sprinkler.util.closeableScope
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.*
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.quarkus.archiver.exception.UnsupportedArchiveException
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.condition.DisabledOnOs
import org.junit.jupiter.api.condition.OS
import kotlin.io.path.*

@QuarkusTest
class ArchiverManagerTest {
    @Inject
    lateinit var archiverManager: ArchiverManager

    @Test
    fun `The number of supported archivers should be equal to the number of implementations of the Archiver interface`() {
        assertEquals(2, archiverManager.supportedArchivers.size)
    }

    @TestFactory
    @OptIn(ExperimentalPathApi::class)
    fun `All supported archive types should successfully archive and unarchive a directory`(): List<DynamicTest> {
        return archiverManager.supportedArchivers.map { archiver ->
            DynamicTest.dynamicTest("Archiver: ${archiver::class.simpleName}") {
                closeableScope {
                    // Create a temporary archive file and unarchive directory
                    val archiveFile = using(EphemeralFile(prefix = "archive-")).path
                    val destinationDirectory = using(EphemeralDirectory(prefix = "unarchive-")).path
                    // Archive and unarchive the example directory
                    val sourceDirectory = getResource("file/example").asPath()

                    archiver.archive(sourceDirectory, archiveFile)
                    archiver.unarchive(archiveFile, destinationDirectory)

                    // List all files and directories in the source and destination directories
                    val sourcePaths = sourceDirectory.walk(PathWalkOption.INCLUDE_DIRECTORIES).toList().sorted()
                    val destinationPaths = destinationDirectory.walk(PathWalkOption.INCLUDE_DIRECTORIES).toList().sorted()

                    // Assert that the unarchived tar contains the same files and directories as the original directory
                    assertEquals(sourcePaths.count(), destinationPaths.count())
                    assertEquals(sourcePaths.map { sourceDirectory.relativize(it) }, destinationPaths.map { destinationDirectory.relativize(it) })

                    // Assert that the content of all files in the unarchived tar is the same as the content of the original directory
                    val originalContents = sourcePaths.filter { !it.isDirectory() }.map { it.readLines() }
                    val unarchivedContents = destinationPaths.filter { !it.isDirectory() }.map { it.readLines() }
                    assertEquals(originalContents.toSet(), unarchivedContents.toSet())
                }
            }
        }
    }

    @DisabledOnOs(OS.WINDOWS)
    @OptIn(ExperimentalPathApi::class)
    @Test
    fun `Archiving a directory and unarchiving the file should preserve the permissions of the files it is archiving`(): Unit = closeableScope {
        // Create a temporary archive file and unarchive directory
        val archiveFile = using(EphemeralFile(prefix = "archive-", suffix = ".tar")).path
        val destinationDirectory = using(EphemeralDirectory(prefix = "unarchive-")).path
        // Archive and unarchive the example directory
        val sourceDirectory = getResource("file/example").asPath()

        archiverManager.archive(sourceDirectory, archiveFile)
        archiverManager.unarchive(archiveFile, destinationDirectory)

        // List all files and directories in the source and destination directories
        val sourcePaths = sourceDirectory.walk(PathWalkOption.INCLUDE_DIRECTORIES).toList().sorted()
        val destinationPaths = destinationDirectory.walk(PathWalkOption.INCLUDE_DIRECTORIES).toList().sorted()

        val sourceFilePermissions = sourcePaths.drop(1).map { it.getPosixFilePermissions() }
        val destinationFilePermissions = destinationPaths.drop(1).map { it.getPosixFilePermissions() }

        assertEquals(sourceFilePermissions, destinationFilePermissions)
    }

    @Test
    fun `Unarchiving an unsupported archive type should result in an UnsupportedArchiveException`(): Unit =
        withEphemeralDirectory { directory ->
            val unsupportedFile = getResource("file/example.tar.gz").asPath()
            assertThrows<UnsupportedArchiveException> { archiverManager.unarchive(unsupportedFile, directory) }
        }

    @TestFactory
    fun `Archiving an unsupported MIME type should throw an UnsupportedArchiveException`(): List<DynamicTest> {
        return archiverManager.supportedArchivers.map { archiver ->
            DynamicTest.dynamicTest("Archiver: ${archiver::class.simpleName}") {
                closeableScope {
                    val sourceDirectory = using(EphemeralDirectory(prefix = "source")).path
                    val destinationArchive = using(EphemeralFile(prefix = "dest", suffix = ".unknown")).path

                    val mimeType = MediaType.parse("application/unknown")

                    assertThrows<UnsupportedArchiveException> {
                        archiverManager.archive(sourceDirectory, destinationArchive, mimeType)
                    }
                }
            }
        }
    }

    @TestFactory
    fun `Archiving a supported MIME type should not throw exceptions`(): List<DynamicTest> {
        return archiverManager.supportedArchivers.map { archiver ->
            DynamicTest.dynamicTest("Archiver: ${archiver::class.simpleName}") {
                closeableScope {
                    val sourceDirectory = using(EphemeralDirectory(prefix = "source")).path
                    val destinationArchive = using(EphemeralFile(prefix = "dest", suffix = ".unknown")).path
                    val mimeType = archiver.supportedMimeTypes.first()

                    archiverManager.archive(sourceDirectory, destinationArchive, mimeType)
                }
            }
        }
    }

    @Test
    fun `Archiving a directory to an unsupported MIME type should throw an UnsupportedArchiveException`(): Unit =
        closeableScope {
            val sourceDirectory = using(EphemeralDirectory(prefix = "source")).path
            val destinationArchive = using(EphemeralFile(prefix = "dest", suffix = ".unknown")).path
            assertThrows<UnsupportedArchiveException> {
                archiverManager.archive(sourceDirectory, destinationArchive)
            }
        }
}
