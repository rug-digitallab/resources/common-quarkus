package nl.rug.digitallab.common.quarkus.archiver.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.condition.DisabledOnOs
import org.junit.jupiter.api.condition.OS
import java.nio.file.attribute.PosixFilePermission

class PosixPermissionsTest {
    private val toPosixFilePermissionsTests: List<ToPosixFilePermissionsTest> = listOf(
        ToPosixFilePermissionsTest(
            "764",
            setOf(
                PosixFilePermission.OWNER_READ,
                PosixFilePermission.OWNER_WRITE,
                PosixFilePermission.OWNER_EXECUTE,
                PosixFilePermission.GROUP_READ,
                PosixFilePermission.GROUP_WRITE,
                PosixFilePermission.OTHERS_READ,
            )
        ),
        ToPosixFilePermissionsTest(
            "760",
            setOf(
                PosixFilePermission.OWNER_READ,
                PosixFilePermission.OWNER_WRITE,
                PosixFilePermission.OWNER_EXECUTE,
                PosixFilePermission.GROUP_READ,
                PosixFilePermission.GROUP_WRITE,
            )
        ),
        ToPosixFilePermissionsTest(
            "777",
            setOf(
                PosixFilePermission.OWNER_READ,
                PosixFilePermission.OWNER_WRITE,
                PosixFilePermission.OWNER_EXECUTE,
                PosixFilePermission.GROUP_READ,
                PosixFilePermission.GROUP_WRITE,
                PosixFilePermission.GROUP_EXECUTE,
                PosixFilePermission.OTHERS_READ,
                PosixFilePermission.OTHERS_WRITE,
                PosixFilePermission.OTHERS_EXECUTE,
            )
        ),
        ToPosixFilePermissionsTest(
            "0", emptySet()
        ),
        ToPosixFilePermissionsTest(
            "467",
            setOf(
                PosixFilePermission.OWNER_READ,
                PosixFilePermission.GROUP_READ,
                PosixFilePermission.GROUP_WRITE,
                PosixFilePermission.OTHERS_READ,
                PosixFilePermission.OTHERS_WRITE,
                PosixFilePermission.OTHERS_EXECUTE
            )
        ),
    )

    private val toIntegerFileModeTests: List<ToOctalFileModeTest> = listOf(
        ToOctalFileModeTest(
            setOf(
                PosixFilePermission.OWNER_READ,
                PosixFilePermission.OWNER_WRITE,
                PosixFilePermission.OWNER_EXECUTE,
                PosixFilePermission.GROUP_READ,
                PosixFilePermission.GROUP_WRITE,
                PosixFilePermission.OTHERS_READ,
            ),
            "764",
        ),
        ToOctalFileModeTest(
            setOf(
                PosixFilePermission.OWNER_READ,
                PosixFilePermission.OWNER_WRITE,
                PosixFilePermission.OWNER_EXECUTE,
                PosixFilePermission.GROUP_READ,
                PosixFilePermission.GROUP_WRITE,
            ),
            "760",
        ),
        ToOctalFileModeTest(
            setOf(
                PosixFilePermission.OWNER_READ,
                PosixFilePermission.OWNER_WRITE,
                PosixFilePermission.OWNER_EXECUTE,
                PosixFilePermission.GROUP_READ,
                PosixFilePermission.GROUP_WRITE,
                PosixFilePermission.GROUP_EXECUTE,
                PosixFilePermission.OTHERS_READ,
                PosixFilePermission.OTHERS_WRITE,
                PosixFilePermission.OTHERS_EXECUTE,
            ),
            "777",
        ),
        ToOctalFileModeTest(
            emptySet(),
            "0",
        ),
        ToOctalFileModeTest(
            setOf(
                PosixFilePermission.OWNER_READ,
                PosixFilePermission.GROUP_READ,
                PosixFilePermission.GROUP_WRITE,
                PosixFilePermission.OTHERS_READ,
                PosixFilePermission.OTHERS_WRITE,
                PosixFilePermission.OTHERS_EXECUTE
            ),
            "467",
        ),
    )

    /**
     * For the following tests, to access toPosixFilePermissions / toIntegerFileMode, we need to place it within an
     * Archiver context. To this end, we create a TarArchiver and place the function calls within that context.
     */

    @TestFactory
    @DisabledOnOs(OS.WINDOWS)
    fun `toPosixFilePermissions correctly converts to the right mode`(): List<DynamicTest> {
        return toPosixFilePermissionsTests.map { test ->
            DynamicTest.dynamicTest("Converting ${test.toTest} to ${test.expected}") {
                assertEquals(test.expected, test.toTest.toPosixFilePermissions())
            }
        }
    }

    @TestFactory
    @DisabledOnOs(OS.WINDOWS)
    fun `toIntegerFileMode correctly converts to the right set of PosixPermissions`(): List<DynamicTest> {
        return toIntegerFileModeTests.map { test ->
            DynamicTest.dynamicTest("Converting ${test.toTest} to ${test.expected}") {
                assertEquals(test.expected, test.toTest.toOctalFileMode())
            }
        }
    }

    /**
     * A helper class to hold data related to testing [toPosixFilePermissions]
     *
     * @property toTest The file mode, as a string. For example, rwxrwxrwx is represented by 777
     * @property expected The set of PosixFilePermissions associated with the mode.
     */
    data class ToPosixFilePermissionsTest(
        val toTest: String,
        val expected: Set<PosixFilePermission>,
    )

    /**
     * A helper class to hold data related to testing [toOctalFileMode]
     *
     * @property toTest The set of PosixFilePermissions to use for the test
     * @property expected The file mode, as a string. For example, rwxrwxrwx is represented by 777, that corresponds to
     * [toTest].
     */
    data class ToOctalFileModeTest(
        val toTest: Set<PosixFilePermission>,
        val expected: String,
    )
}
