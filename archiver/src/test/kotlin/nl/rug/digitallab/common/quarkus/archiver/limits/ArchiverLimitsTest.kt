package nl.rug.digitallab.common.quarkus.archiver.limits

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.quarkus.archiver.ArchiverManager
import nl.rug.digitallab.common.quarkus.archiver.exception.*
import org.junit.jupiter.api.*
import kotlin.io.path.*

@QuarkusTest
class ArchiverLimitsTest {
    @Inject
    lateinit var archiverManager: ArchiverManager

    @TestFactory
    fun `Unarchiving zip bombs should throw an inflation exception`(): List<DynamicTest> {
        val bombs = getResource("file/limits").asPath()
        return bombs
            .listDirectoryEntries()
            .filter { it.name.startsWith("zipbomb") }
            .map { fakeBomb ->
                DynamicTest.dynamicTest(
                    "Bomb with name: ${fakeBomb.fileName.nameWithoutExtension}"
                ) {
                    // Strip the "fake" extension to unleash the beast
                    // These files have this extension to prevent Gradle from trying to read them,
                    // and apparently Gradle hangs on the larger zip bombs.
                    val bomb = fakeBomb.moveTo(Path(fakeBomb.fileName.nameWithoutExtension))

                    // Trying to unarchive the file should fail
                    try {
                        withEphemeralDirectory { directory ->
                            assertThrows<ArchiveInflationException> {
                                archiverManager.unarchive(bomb, directory)
                            }
                        }
                    } finally {
                        // In any case, we clean up the bomb
                        bomb.deleteIfExists()
                    }
                }
            }
    }

    @Test
    fun `Unarchiving a zip with more than 1000 entries should fail`(): Unit = withEphemeralDirectory { directory ->
        val zip = getResource("file/limits/filecount.zip").asPath()

        assertThrows<TooManyFilesException> { archiverManager.unarchive(zip, directory) }
    }

    @Test
    fun `Unarchiving a zip that is too large when uncompressed should fail`(): Unit =
        withEphemeralDirectory { directory ->
            val zip = getResource("file/limits/entrysize.zip").asPath()

            assertThrows<ArchiveEntryTooLargeException> { archiverManager.unarchive(zip, directory) }
        }
}
