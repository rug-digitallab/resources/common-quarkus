package nl.rug.digitallab.common.quarkus.test.rest

import org.apache.commons.io.FileUtils
import org.jboss.resteasy.reactive.multipart.FileUpload
import java.io.File
import java.nio.file.Path

/**
 * A mock implementation of the [FileUpload] interface.
 *
 * @param file The file to mock the [FileUpload] for.
 */
open class MockFileUpload(private val file: File): FileUpload {
    /**
     * Create a new [MockFileUpload] from a [Path].
     *
     * @param file The file to mock the [FileUpload] for.
     */
    constructor(file: Path) : this(file.toFile())

    override fun name(): String = file.name

    override fun filePath(): Path? = file.toPath()

    override fun fileName(): String = file.name

    override fun size(): Long = FileUtils.sizeOf(file)

    override fun contentType(): String = "application/octet-stream"

    override fun charSet(): String = "UTF-8"
}

/**
 * A mock implementation of the [FileUpload] interface that returns null for the [filePath] method. There is an edge
 * case in RestEasy Reactive where the [filePath] method can return null, so this class is used to test that edge case.
 */
class MockFileUploadNullPath(file: File): MockFileUpload(file) {
    constructor(file: Path) : this(file.toFile())

    override fun filePath(): Path? = null
}
