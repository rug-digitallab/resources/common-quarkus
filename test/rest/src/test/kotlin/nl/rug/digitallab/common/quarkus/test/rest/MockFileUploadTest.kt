package nl.rug.digitallab.common.quarkus.test.rest

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.common.kotlin.helpers.resource.asFile
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class MockFileUploadTest {
    @Test
    fun `A MockFileUpload constructed from a file should return the expected data`() {
        val testResource = getResource("test.txt")

        listOf(
            MockFileUpload(testResource.asPath()),
            MockFileUpload(testResource.asFile()),
        ).forEach { fileUpload ->
            assertEquals("test.txt", fileUpload.name())
            assertEquals("test.txt", fileUpload.fileName())
            assertEquals("application/octet-stream", fileUpload.contentType())
            assertEquals("UTF-8", fileUpload.charSet())
            assertEquals(539, fileUpload.size())
            assertEquals(testResource.asPath(), fileUpload.filePath())
            assertEquals(testResource.asPath(), fileUpload.uploadedFile())
        }
    }

    @Test
    fun `A MockFileUploadNullPath constructed from a file should return the expected data`() {
        val testResource = getResource("test.txt")

        listOf(
            MockFileUploadNullPath(testResource.asPath()),
            MockFileUploadNullPath(testResource.asFile()),
        ).forEach { fileUpload ->
            assertEquals("test.txt", fileUpload.name())
            assertEquals("test.txt", fileUpload.fileName())
            assertEquals("application/octet-stream", fileUpload.contentType())
            assertEquals("UTF-8", fileUpload.charSet())
            assertEquals(539, fileUpload.size())
            assertEquals(null, fileUpload.filePath())
            assertEquals(null, fileUpload.uploadedFile())
        }
    }
}
