# Test: REST

This library provides utilities for testing Quarkus REST services.

## Prerequisites

- Quarkus REST

## Usage

```kotlin
val someFile = File("someFile.txt")
val mockFileUpload = MockFileUpload(someFile)
// Use the MockFileUpload in your test
```
