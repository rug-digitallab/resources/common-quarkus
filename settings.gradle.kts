rootProject.name = "common-quarkus"

pluginManagement {
    plugins {
        val digitalLabGradlePluginVersion: String by settings

        id("nl.rug.digitallab.gradle.plugin.quarkus.library") version digitalLabGradlePluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Maven
        gradlePluginPortal()
    }
}

include("archiver")
include("dynamic-bean-registration")
include("exception-mapper:hibernate")
project(":exception-mapper:hibernate").name = "exception-mapper-hibernate"
include("exception-mapper:rest")
project(":exception-mapper:rest").name = "exception-mapper-rest"
include("hibernate:orm")
project(":hibernate:orm").name = "hibernate-orm"
include("hibernate:reactive")
project(":hibernate:reactive").name = "hibernate-reactive"
include("hibernate:validator")
project(":hibernate:validator").name = "hibernate-validator"
include("jackson")
include("jackson:test:csv")
include("jackson:test:json")
include("jackson:test:yaml")
include("jackson:test:rest")
include("opentelemetry")
include("test:rest")
project(":test:rest").name = "test-rest"
