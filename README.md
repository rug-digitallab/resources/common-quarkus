# Common Quarkus

Common Quarkus consists of several libraries that are shared between Digital Lab Quarkus projects. Currently, the 
following libraries are included:
- [Archiver](archiver/README.md)
- [Exception Mapper Hibernate](exception-mapper/hibernate/README.md)
- [Exception Mapper REST](exception-mapper/rest/README.md)
- [Hibernate](hibernate/README.md)
- [Jackson](jackson/README.md)
- [OpenTelemetry](opentelemetry/README.md)
- [Test REST](test/rest/README.md)
