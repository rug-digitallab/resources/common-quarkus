plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    testImplementation(project(":jackson"))

    testImplementation("io.quarkus:quarkus-jackson")
    testImplementation("com.fasterxml.jackson.dataformat:jackson-dataformat-csv")
}
