package nl.rug.digitallab.common.quarkus.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.json.JsonMapper
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
class ProducerTest {
    @Inject lateinit var defaultMapper: ObjectMapper
    @Inject lateinit var jsonMapper1: JsonMapper
    @Inject lateinit var jsonMapper2: JsonMapper

    @Test
    fun `The default mapper should be a JSON mapper`() {
        Assertions.assertInstanceOf(JsonMapper::class.java, defaultMapper)
    }

    @Test
    fun `Mappers are singletons`() {
        Assertions.assertSame(jsonMapper1, jsonMapper2)
    }
}
