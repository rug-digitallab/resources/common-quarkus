package nl.rug.digitallab.common.quarkus.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
class ProducerTest {
    @Inject lateinit var defaultMapper: ObjectMapper
    @Inject lateinit var jsonMapper1: JsonMapper
    @Inject lateinit var jsonMapper2: JsonMapper
    @Inject lateinit var yamlMapper1: YAMLMapper
    @Inject lateinit var yamlMapper2: YAMLMapper

    @Test
    fun `The default mapper should be a JSON mapper`() {
        Assertions.assertInstanceOf(JsonMapper::class.java, defaultMapper)
    }

    @Test
    fun `Mappers are singletons`() {
        Assertions.assertSame(jsonMapper1, jsonMapper2)
        Assertions.assertSame(yamlMapper1, yamlMapper2)
    }

    @Test
    fun `The YAML mapper should resolve anchors`() {
        val normalYaml = """
            foo: foobar
            bar: 123
        """.trimIndent()

        val anchorYaml = """
            .template: &anchor "foobar"
            foo: *anchor
            bar: 123
        """.trimIndent()

        val expected = SimpleSerializable("foobar", 123)

        val parsedNormal = yamlMapper1.readValue(normalYaml, SimpleSerializable::class.java)
        Assertions.assertEquals(expected, parsedNormal)

        val parsedAnchor = yamlMapper1.readValue(anchorYaml, SimpleSerializable::class.java)
        Assertions.assertEquals(expected, parsedAnchor)
    }

    data class SimpleSerializable(
        val foo: String,
        val bar: Int,
    )
}
