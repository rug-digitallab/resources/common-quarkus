plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    val mockitoKotlinVersion: String by project
    
    testImplementation(project(":jackson"))

    testImplementation("io.quarkus:quarkus-jackson")
    
    testImplementation("io.quarkus:quarkus-rest")
    testImplementation("io.quarkus:quarkus-rest-jackson")
    
    testImplementation("io.rest-assured:kotlin-extensions")
    testImplementation("io.rest-assured:rest-assured")
    
    testImplementation("io.quarkus:quarkus-junit5-mockito")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$mockitoKotlinVersion")
    testImplementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")
}
