package nl.rug.digitallab.common.quarkus.jackson

import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import kotlin.time.Duration

@Path("test-resource")
class TestResource {
    data class DurationContainer(val duration: Duration)
    
    @POST
    @Path("duration")
    @Produces("application/json")
    fun durationEndpoint(duration: DurationContainer): DurationContainer {
        return duration
    }
}
