package nl.rug.digitallab.common.quarkus.jackson

import io.quarkus.test.junit.QuarkusTest
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.hamcrest.CoreMatchers.equalTo
import org.jboss.resteasy.reactive.RestResponse.Status
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.time.Duration

@QuarkusTest
class RestTest {
    @Test
    fun `Calling an endpoint with a duration string properly returns it`() {
        Given {
            body("{ \"duration\": \"PT1S\" }")
            contentType("application/json")
        } When {
            post("/test-resource/duration")
        } Then {
            statusCode(Status.OK.statusCode)
            body("duration", equalTo("PT1S"))
        }
    }
    
    @Test
    fun `Calling an endpoint with a duration instance properly returns it`() {
        val duration = Duration.parse("PT1S")
        
        Given { 
            body(TestResource.DurationContainer(duration))
            contentType("application/json")
        } When { 
            post("/test-resource/duration")
        } Then {
            statusCode(Status.OK.statusCode)
            Assertions.assertEquals(duration, extract().`as`(TestResource.DurationContainer::class.java).duration)
        }
    }
}
