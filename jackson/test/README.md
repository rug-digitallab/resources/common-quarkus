# Tests for Jackson Library

There are three small testing-only libraries for the Jackson integration here; they intend to test inclusion of the Jackson library in different environment with different classpaths: only JSON available, or JSON + YAML/JSON + CSV available.

These tests are intended to ensure that the dynamic bean registration works properly.
