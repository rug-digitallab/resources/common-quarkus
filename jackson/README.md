# Jackson
The Jackson library provides a modern way of providing and customizing a variety of ObjectMappers to Quarkus applications. Specifically, it exposes a generic interface that allows for customization of the mappers anywhere in their construction. Additionally, the library implements some Digital Lab-specific default conventions, as well as support for YAML Anchors.

## Prerequisites

- Quarkus Jackson

## Usage

The code from this library is fully backwards-compatible with the normal ObjectMappers from Quarkus. After including the library in your classpath, the instantiation will be taken over by this library.

### Using custom mappers

For many use cases, the default behaviour should suffice: it will return a JSON mapper with all the needed configuration. However, this library also offers a custom YAML mapper, and can support more mapper types in the future. You can use these custom mappers by annotating the injection site of `ObjectMapper` with the `@Mapper(Mapper.Type.XXXX)` annotation. Based on the given type, the correct mapper will automatically be returned.

In addition, it is possible to directly inject specialised mapper types, such as `JsonMapper` and `YAMLMapper`.

### Using YAML Anchors

Currently, support for YAML Anchors is dependent on unreleased features in SnakeYAML. The YAML Anchor support will automatically be activated once the correct version of SnakeYAML is on the classpath. To do so, include the following definitions in your `build.gradle.kts`:

```kotlin
// Override Quarkus-provided version of SnakeYAML to be the snapshot version
configurations.all {
    resolutionStrategy {
        force("org.yaml:snakeyaml:2.3")
    }
}
```

### Customizing the mappers

This library also supports external customization of the produced mappers. This is achieved through implementing `BuilderCustomizer` for the appropriate type - this depends on the phase of customization, and on the desired targeted mappers.

For example, to implement a customizer of any mapper in the `MapperBuilder` phase (the most common usecase):

```kotlin
// Annotations for CDI
@Dependent @Unremovable
class Customizer<B : MapperBuilder<*, B>> : BuilderCustomizer<B> {
    override fun customize(builder: B) {
        ...
    }
}
```

It is also possible to provide implementations of the Quarkus-style `ObjectMapperCustomizer` interface, any such implementations will still be picked up and applied. However, this interface only allows access to now-deprecated customization methods, and is thus no longer recommended.
