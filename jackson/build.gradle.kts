val snakeYamlVersion: String by project

fun DependencyHandler.hiddenImplementation(dependencyNotation: Any) {
    compileOnly(dependencyNotation)
    testImplementation(dependencyNotation)
}

plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    api("io.quarkus:quarkus-jackson")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation(project(":dynamic-bean-registration"))

    hiddenImplementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")
    hiddenImplementation("com.fasterxml.jackson.dataformat:jackson-dataformat-csv")
    hiddenImplementation("org.yaml:snakeyaml:$snakeYamlVersion")
    hiddenImplementation("io.quarkus:quarkus-junit5")
    hiddenImplementation("io.rest-assured:rest-assured")
}

// The particular dereferencing feature from SnakeYAML is only available
// from version 2.3. We force this version to be used, since Jackson
// specifies an older version.
configurations.all {
    resolutionStrategy {
        force("org.yaml:snakeyaml:$snakeYamlVersion")
    }
}
