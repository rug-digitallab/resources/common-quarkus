package nl.rug.digitallab.common.quarkus.jackson.customizers

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.cfg.MapperBuilder
import com.fasterxml.jackson.module.kotlin.KotlinFeature
import com.fasterxml.jackson.module.kotlin.kotlinModule
import io.quarkus.arc.Unremovable
import jakarta.enterprise.context.Dependent
import java.time.Duration
import java.time.temporal.ChronoUnit

/**
 * Customizer of [MapperBuilder] to configure some Digital Lab-specific
 * conventions for JSON and YAML parsing. In particular, we configure
 * enums and properties to be case-insensitive, we register Kotlin
 * support, and we configure [Duration]s that are represented by integers
 * to be interpreted as milliseconds.
 *
 * @param BuilderT The concrete [MapperBuilder] implementation, auto-resolved by CDI
 */
@Dependent
@Unremovable
class DigitalLabCustomizer<BuilderT : MapperBuilder<*, BuilderT>> : BuilderCustomizer<BuilderT> {
    override fun customize(builder: BuilderT) {
        // We enable case-insensitivity of enums, properties and values. Technically,
        // JSON is case-sensitive, but not all implementations adhere to this, so
        // it's better to be more lenient in terms of what we expect and accept.
        // This is also more user-friendly.
        builder.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
        builder.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
        builder.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_VALUES)

        // We always want Kotlin support to be present
        builder.addModule(kotlinModule {
            enable(KotlinFeature.UseJavaDurationConversion)
        })

        // DL Convention: when fields of type [Duration] are represented as
        // an integer in JSON/YAML, we interpret it as being in milliseconds.
        // This is an _additional_ accepted format, as normally durations are
        // only accepted as strings in ISO 8601 format (e.g. "PT1M"). This
        // only applies to deserialization.
        builder.withConfigOverride(Duration::class.java) {
            it.setFormat(JsonFormat.Value.forPattern(ChronoUnit.MILLIS.name))
        }
        // After adding the numeric value, we need to disable serializing
        // to that representation.
        builder.disable(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS)
    }
}
