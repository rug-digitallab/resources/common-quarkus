package nl.rug.digitallab.common.quarkus.jackson

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonFactoryBuilder
import com.fasterxml.jackson.core.TSFBuilder
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.cfg.MapperBuilder
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.dataformat.csv.CsvFactory
import com.fasterxml.jackson.dataformat.csv.CsvFactoryBuilder
import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.dataformat.yaml.YAMLFactoryBuilder
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import io.quarkus.arc.All
import io.quarkus.jackson.ObjectMapperCustomizer
import jakarta.enterprise.inject.Default
import jakarta.enterprise.inject.Produces
import jakarta.enterprise.inject.Typed
import jakarta.inject.Inject
import jakarta.inject.Singleton
import nl.rug.digitallab.common.quarkus.helper.IfClass
import nl.rug.digitallab.common.quarkus.jackson.customizers.BuilderCustomizer

/**
 * Logic for producing JSON and YAML mappers, fully incorporating all
 * possible customization throughout the construction of these mappers.
 *
 * The logic here supersedes and replaces default logic present in Quarkus,
 * but contains all rules from Quarkus. The customization does include all
 * "legacy" customization offered through Quarkus' [ObjectMapperCustomizer].
 *
 * In particular, it creates mappers for both JSON and YAML, that can be
 * injected through their specialised subtypes ([JsonMapper] and [YAMLMapper]),
 * or by qualified injections of their supertype ([ObjectMapper]). An
 * unqualified (Default) injection of the supertype will default to a JSON
 * mapper, to stay compatible with normal Quarkus behaviour.
 */

class JsonMapperProducer : BaseObjectMapperProducer() {
    /**
     * Produces a [JsonMapper] that is the default implementation for both
     * [JsonMapper] and [ObjectMapper] injections.
     */
    @Produces
    @Singleton
    @Default
    @IfClass(JsonMapper::class)
    fun produceJsonMapper(
        // We look for all factory builder and mapper builder customizers
        @All factoryBuilderCustomizers: MutableList<BuilderCustomizer<JsonFactoryBuilder>>,
        @All mapperBuilderCustomizers: MutableList<BuilderCustomizer<JsonMapper.Builder>>,
    ): JsonMapper {
        return produceObjectMapper(factoryBuilderCustomizers, mapperBuilderCustomizers, {
            JsonFactoryBuilder(JsonFactory())
        }, {
            JsonMapper.builder(it)
        })
    }
}

class YamlMapperProducer : BaseObjectMapperProducer() {
    /**
     * Produces a [YAMLMapper] that is the default implementation only for
     * [YAMLMapper] itself, but will not by default be injected for [ObjectMapper].
     */
    @Produces
    @Singleton
    @Default
    @Typed(YAMLMapper::class)
    @IfClass(YAMLMapper::class)
    fun produceYamlMapper(
        // We look for all factory builder and mapper builder customizers
        @All factoryBuilderCustomizers: MutableList<BuilderCustomizer<YAMLFactoryBuilder>>,
        @All mapperBuilderCustomizers: MutableList<BuilderCustomizer<YAMLMapper.Builder>>,
    ): YAMLMapper {
        return produceObjectMapper(factoryBuilderCustomizers, mapperBuilderCustomizers, {
            YAMLFactoryBuilder(YAMLFactory())
        }, {
            YAMLMapper.builder(it)
        })
    }
}

class CsvMapperProducer : BaseObjectMapperProducer() {
    /**
     * Produces a [CsvMapper] that is the default implementation only for
     * [CsvMapper] itself, but will not by default be injected for [ObjectMapper].
     */
    @Produces
    @Singleton
    @Default
    @Typed(CsvMapper::class)
    @IfClass(CsvMapper::class)
    fun produceCsvMapper(
        @All factoryBuilderCustomizers: MutableList<BuilderCustomizer<CsvFactoryBuilder>>,
        @All mapperBuilderCustomizers: MutableList<BuilderCustomizer<CsvMapper.Builder>>,
    ): CsvMapper {
        return produceObjectMapper(factoryBuilderCustomizers, mapperBuilderCustomizers, {
            CsvFactoryBuilder(CsvFactory())
        }, {
            CsvMapper.builder(it)
        })
    }
}

abstract class BaseObjectMapperProducer {
    // We still support the old-style Quarkus-specific customization
    @All
    @Inject
    protected lateinit var mapperCustomizers: MutableList<ObjectMapperCustomizer>

    /**
     * Generic function for constructing [ObjectMapper] instances. The implementation
     * makes sure that all possible customizers are applied, in order, to all steps in
     * the construction: FactoryBuilder, MapperBuilder, and Mapper itself.
     *
     * @param factoryBuilderCustomizers All customizers for the relevant factory builder
     * @param mapperBuilderCustomizers All customizers for the relevant mapper builder
     * @param factoryBuilderCreator Function to instantiate the factory builder
     * @param mapperBuilderCreator Function to instantiate the mapper builder, given the built factory
     *
     * @return Fully configured [ObjectMapper] subtype
     */
    protected fun <FactoryT, FactoryBuilderT, MapperT, MapperBuilderT> produceObjectMapper(
        factoryBuilderCustomizers: MutableList<BuilderCustomizer<FactoryBuilderT>>,
        mapperBuilderCustomizers: MutableList<BuilderCustomizer<MapperBuilderT>>,
        factoryBuilderCreator: () -> FactoryBuilderT,
        mapperBuilderCreator: (FactoryT) -> MapperBuilderT
    ): MapperT
        where
            FactoryT : JsonFactory,
            FactoryBuilderT : TSFBuilder<FactoryT, FactoryBuilderT>,
            MapperT : ObjectMapper,
            MapperBuilderT : MapperBuilder<MapperT, MapperBuilderT>
    {
        // First, we construct the factory builder and apply all customizations to it
        val factoryBuilder = factoryBuilderCreator()

        sortBuilderCustomizers(factoryBuilderCustomizers).forEach {
            it.customize(factoryBuilder)
        }

        // Then, we build the factory, construct a mapper builder using it, and apply
        // all customizations to the mapper builder.
        val factory = factoryBuilder.build()
        val mapperBuilder = mapperBuilderCreator(factory)

        sortBuilderCustomizers(mapperBuilderCustomizers).forEach {
            it.customize(mapperBuilder)
        }

        // Then, we build the mapper, and we apply all "legacy" customizations to it.
        val mapper = mapperBuilder.build()

        sortObjectMapperCustomizers(mapperCustomizers).forEach {
            it.customize(mapper)
        }

        return mapper
    }

    private fun sortObjectMapperCustomizers(customizers: List<ObjectMapperCustomizer>) =
        customizers.sortedByDescending { it.priority() }

    private fun <T> sortBuilderCustomizers(customizers: List<BuilderCustomizer<T>>) =
        customizers.sortedByDescending { it.priority() }
}
