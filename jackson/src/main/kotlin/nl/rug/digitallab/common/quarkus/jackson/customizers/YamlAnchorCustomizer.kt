package nl.rug.digitallab.common.quarkus.jackson.customizers

import com.fasterxml.jackson.dataformat.yaml.YAMLFactoryBuilder
import io.quarkus.arc.Unremovable
import jakarta.inject.Singleton
import nl.rug.digitallab.common.quarkus.helper.IfClass
import nl.rug.digitallab.common.quarkus.helper.IfMethod
import nl.rug.digitallab.common.quarkus.jackson.yaml.YamlInputDecorator
import org.yaml.snakeyaml.DumperOptions

/**
 * Customizer of [YAMLFactoryBuilder] to add support for YAML Anchors
 * through [YamlInputDecorator]. The [YAMLFactoryBuilder] is then used
 * to construct a [YAMLFactory], which is the basis for constructing
 * a [YAMLMapper] through the [YAMLMapper.Builder].
 *
 * NOTE: The anchor feature is dependent on the new [DumperOptions::setDereferenceAliases]
 * function.
 */
@Singleton
@Unremovable
@IfClass(YAMLFactoryBuilder::class)
@IfMethod(DumperOptions::class, "setDereferenceAliases")
class YamlAnchorCustomizer : BuilderCustomizer<YAMLFactoryBuilder> {
    override fun customize(builder: YAMLFactoryBuilder) {
        builder.inputDecorator(YamlInputDecorator)
    }
}
