package nl.rug.digitallab.common.quarkus.jackson.yaml

import com.fasterxml.jackson.core.io.IOContext
import com.fasterxml.jackson.core.io.InputDecorator
import com.fasterxml.jackson.dataformat.yaml.JacksonYAMLParseException
import nl.rug.digitallab.common.quarkus.jackson.yaml.YamlInputDecorator.YamlMergeResolver.flattenMapping
import nl.rug.digitallab.common.quarkus.jackson.yaml.YamlInputDecorator.YamlMergeResolver.recursivelyFlattenMappings
import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.LoaderOptions
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.SafeConstructor
import org.yaml.snakeyaml.error.MarkedYAMLException
import org.yaml.snakeyaml.error.YAMLException
import org.yaml.snakeyaml.nodes.MappingNode
import org.yaml.snakeyaml.nodes.Node
import org.yaml.snakeyaml.nodes.SequenceNode
import java.io.InputStream
import java.io.Reader
import java.io.StringWriter
import com.fasterxml.jackson.dataformat.yaml.snakeyaml.error.MarkedYAMLException as JacksonMarkedYAMLException
import com.fasterxml.jackson.dataformat.yaml.snakeyaml.error.YAMLException as JacksonYAMLException

/**
 * [InputDecorator] implementation specific for YAML that will
 * preprocess YAML input to resolve YAML Anchors and merge YAML mergers.
 *
 * By default, Jackson does not support YAML Anchors or mergers as it does
 * not properly resolve the Anchor references in the tree representation.
 * It is somewhat unclear why this is not done properly, but it seems
 * to be related to a desire to be able to reproduce the exact same YAML
 * after re-serialization of the deserialized result. This same desire
 * can be found in SnakeYAML (the underlying YAML parser for Jackson).
 *
 * This implementation will pre-process the YAML string by parsing it
 * into a tree representation through SnakeYAML, manually resolving the
 * mergers by re-using existing library code, and then re-serializing
 * that to a string. This relies on the "dereference aliases" option.
 *
 * This [InputDecorator] can be registered by passing it to the
 * [YAMLFactoryBuilder], as is done in the [YamlAnchorCustomizer].
 */
object YamlInputDecorator : InputDecorator() {
    private fun resolveYaml(yamlSource: Reader): InputStream {
        // We construct a SnakeYAML engine with the proper dumping options
        val yamlDumperOptions = DumperOptions().apply {
            isDereferenceAliases = true
        }
        val yamlEngine = Yaml(yamlDumperOptions)

        // We can only serialize to a Writer
        val writer = StringWriter()

        // Deserialize partially (not to JVM objects, but to an
        // intermediate tree representation), then flatten all
        // maps to resolve the mergers, and then re-serialize
        // to a string while dereferencing the anchors.
        try {
            val sourceTree = yamlEngine.compose(yamlSource)
            sourceTree.recursivelyFlattenMappings()
            yamlEngine.serialize(sourceTree, writer)
        } catch (ex: Exception) {
            when(ex) {
                is MarkedYAMLException -> throw JacksonMarkedYAMLException.from(null, ex)
                is YAMLException -> throw JacksonYAMLException.from(null, ex)
                else -> throw JacksonYAMLParseException(null, ex.message, ex)
            }
        }

        return writer.toString().byteInputStream()
    }

    override fun decorate(ctxt: IOContext, input: InputStream): InputStream {
        return resolveYaml(input.reader())
    }

    override fun decorate(ctxt: IOContext, input: ByteArray, offset: Int, length: Int): InputStream {
        return resolveYaml(input.inputStream(offset, length).reader())
    }

    override fun decorate(ctxt: IOContext, input: Reader): Reader {
        return resolveYaml(input).reader()
    }

    /**
     * Helper object to recursively call [flattenMapping] on every
     * [MappingNode] in a given YAML [Node] representation.
     *
     * Normally, the flattening of mergers only happens upon JVM
     * object instance construction. However, our use case needs
     * to flatten the maps before that, as we are not able to
     * construct JVM objects since we deal with generic YAML data.
     *
     * The code implementing the flattening process is contained in
     * the [SafeConstructor] class, and is a protected, stateless
     * and thus effectively static method. To be able to access this
     * method, we subclass [SafeConstructor] so we can expose this
     * logic in a custom [recursivelyFlattenMappings] method.
     *
     * This is somewhat hacky, but better than reimplementing the
     * code ourselves. In addition, the [SafeConstructor] is very
     * likely to remain stateless due to the code architecture.
     *
     * For completeness, there is an issue in the SnakeYAML repo
     * to properly implement the flattening through a [DumperOptions]
     * options.
     * https://bitbucket.org/snakeyaml/snakeyaml/issues/1096/dump-with-resolved-mergers
     */
    private object YamlMergeResolver : SafeConstructor(LoaderOptions()) {

        /**
         * Recursively calls [flattenMapping] on the given node, resolving
         * all mergers in all maps in the node tree.
         */
        fun Node.recursivelyFlattenMappings() {
            // For maps, we call the flatten function to process any mergers
            if(this is MappingNode && this.isMerged)
                flattenMapping(this)

            // We recurse down the object tree for maps and lists (sequences)
            if(this is MappingNode)
                this.value.forEach { it.valueNode.recursivelyFlattenMappings() }

            if(this is SequenceNode)
                this.value.forEach { it.recursivelyFlattenMappings() }
        }
    }
}
