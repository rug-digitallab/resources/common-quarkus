package nl.rug.digitallab.common.quarkus.jackson.integrations.restassured

import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.arc.Arc
import io.quarkus.test.junit.callback.QuarkusTestBeforeEachCallback
import io.quarkus.test.junit.callback.QuarkusTestMethodContext
import io.restassured.RestAssured
import io.restassured.config.ObjectMapperConfig

/**
 * Custom [QuarkusTestBeforeEachCallback] to properly configure RestAssured to
 * use our custom Jackson [ObjectMapper], instead of some random statically-
 * initialized version.
 * 
 * Unfortunately, RestAssured can only be configured through static variables,
 * so no neat injection can be done. This callback cannot be "conditionally
 * registered", so to prevent an implicit dependency on RestAssured in all
 * test suites, we need to manually check for the presence of RestAssured
 * on the classpath.
 */
class RestAssuredObjectMapperConfiguration : QuarkusTestBeforeEachCallback {
    override fun beforeEach(context: QuarkusTestMethodContext) {
        if(!RestAssuredPresent)
            return
        
        // If we know that RestAssured is present, we will obtain an instance
        // of our mapper, and tell RestAssured to use it.
        val mapper = Arc.container().instance(ObjectMapper::class.java).get()
        RestAssured.config = RestAssured
            .config()
            .objectMapperConfig(
                ObjectMapperConfig().jackson2ObjectMapperFactory { _, _ -> mapper }
            )
    }
    
    companion object {
        /**
         * Lazily checks whether RestAssured is present on the current classpath
         */
        private val RestAssuredPresent: Boolean by lazy { 
            try {
                Class.forName("io.restassured.RestAssured")
                true
            } catch (_: ClassNotFoundException) {
                false
            }
        }
    }
}
