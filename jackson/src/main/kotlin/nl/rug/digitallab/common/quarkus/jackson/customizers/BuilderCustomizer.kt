package nl.rug.digitallab.common.quarkus.jackson.customizers

/**
 * Simple interface specifying a generic "customizer" of some kind of
 * builder class. The implementation here is not specific to builders
 * of any kind, but its usage is intended for Jackson-related builders
 * such as the MapperBuilder and TSFBuilder.
 *
 * The interface is pre-equipped with a priority system to determine
 * the order of customization applications.
 *
 * NOTE: Implementations of this interface should be annotated with
 * [Unremovable] to prevent Quarkus from pruning the customizer from
 * the CDI container.
 *
 * Since most builder interfaces have a recursive type definition,
 * implementing this class for a generic builder interface is somewhat
 * tricky. Consider the following example:
 * ```kotlin
 * // Recursive builder type
 * interface Builder<T, B : Builder<T, B>> { ... }
 *
 * // Customizer for all such Builder implementations
 * @Dependent @Unremovable
 * class Customizer<B : Builder<*, B>> : BuilderCustomizer<B> { ... }
 * ```
 *
 * @param BuilderT The builder type that is targeted for customization
 */
interface BuilderCustomizer<BuilderT>
    : Comparable<BuilderCustomizer<BuilderT>>
{
    /**
     * Apply the customization logic to the given [BuilderT]
     *
     * @param builder The builder to apply the customization to
     */
    fun customize(builder: BuilderT)

    /**
     * The priority of the customizer, used to determine the order
     * of customization applications. Overriding is only needed
     * when conflicts would arise otherwise.
     *
     * @return The priority of the current customizer; higher number is higher priority
     */
    fun priority() = DEFAULT_PRIORITY

    override fun compareTo(other: BuilderCustomizer<BuilderT>) =
        other.priority().compareTo(priority())

    companion object {
        const val DEFAULT_PRIORITY = 0
        const val MINIMUM_PRIORITY = Integer.MIN_VALUE
        const val MAXIMUM_PRIORITY = Integer.MAX_VALUE
    }
}
