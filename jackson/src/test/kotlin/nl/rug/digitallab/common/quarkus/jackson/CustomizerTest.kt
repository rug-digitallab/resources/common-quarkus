package nl.rug.digitallab.common.quarkus.jackson

import com.fasterxml.jackson.core.JsonFactoryBuilder
import com.fasterxml.jackson.core.TSFBuilder
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.cfg.MapperBuilder
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactoryBuilder
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import io.quarkus.jackson.ObjectMapperCustomizer
import io.quarkus.test.junit.QuarkusTest
import jakarta.enterprise.context.Dependent
import jakarta.inject.Inject
import jakarta.inject.Singleton
import nl.rug.digitallab.common.quarkus.jackson.customizers.BuilderCustomizer
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test

@QuarkusTest
class CustomizerTest {
    @Inject lateinit var jsonMapper: JsonMapper
    @Inject lateinit var yamlMapper: YAMLMapper

    @Test
    @Inject
    fun `Customizers should be executed for each mapper`() {
        Assertions.assertEquals(2, GenericFactoryCustomizer.log.size)
        Assertions.assertEquals(1, JsonFactoryCustomizer.log.size)
        Assertions.assertEquals(1, YamlFactoryCustomizer.log.size)
        Assertions.assertEquals(2, GenericMapperCustomizer.log.size)
        Assertions.assertEquals(1, JsonMapperCustomizer.log.size)
        Assertions.assertEquals(1, YamlMapperCustomizer.log.size)
        Assertions.assertEquals(2, LegacyMapperCustomizer.log.size)
    }

    @Dependent
    class GenericFactoryCustomizer <BuilderT : TSFBuilder<*, BuilderT>> : BuilderCustomizer<BuilderT> {
        override fun customize(builder: BuilderT) {
            log += builder.hashCode()
        }

        companion object {
            val log = mutableListOf<Int>()
        }
    }

    @Singleton
    class JsonFactoryCustomizer : BuilderCustomizer<JsonFactoryBuilder> {
        override fun customize(builder: JsonFactoryBuilder) {
            log += builder.hashCode()
        }

        companion object {
            val log = mutableListOf<Int>()
        }
    }

    @Singleton
    class YamlFactoryCustomizer : BuilderCustomizer<YAMLFactoryBuilder> {
        override fun customize(builder: YAMLFactoryBuilder) {
            log += builder.hashCode()
        }

        companion object {
            val log = mutableListOf<Int>()
        }
    }

    @Dependent
    class GenericMapperCustomizer <BuilderT : MapperBuilder<*, BuilderT>> : BuilderCustomizer<BuilderT> {
        override fun customize(builder: BuilderT) {
            log += builder.hashCode()
        }

        companion object {
            val log = mutableListOf<Int>()
        }
    }

    @Singleton
    class JsonMapperCustomizer : BuilderCustomizer<JsonMapper.Builder> {
        override fun customize(builder: JsonMapper.Builder) {
            log += builder.hashCode()
        }

        companion object {
            val log = mutableListOf<Int>()
        }
    }

    @Singleton
    class YamlMapperCustomizer : BuilderCustomizer<YAMLMapper.Builder> {
        override fun customize(builder: YAMLMapper.Builder) {
            log += builder.hashCode()
        }

        companion object {
            val log = mutableListOf<Int>()
        }
    }

    @Singleton
    class LegacyMapperCustomizer : ObjectMapperCustomizer {
        override fun customize(objectMapper: ObjectMapper?) {
            log += objectMapper.hashCode()
        }

        companion object {
            val log = mutableListOf<Int>()
        }
    }

    companion object {
        @JvmStatic
        @BeforeAll
        fun logCleanup() {
            GenericFactoryCustomizer.log.clear()
            JsonFactoryCustomizer.log.clear()
            YamlFactoryCustomizer.log.clear()
            GenericMapperCustomizer.log.clear()
            JsonMapperCustomizer.log.clear()
            YamlMapperCustomizer.log.clear()
            LegacyMapperCustomizer.log.clear()
        }
    }
}
