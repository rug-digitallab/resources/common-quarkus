package nl.rug.digitallab.common.quarkus.jackson.customizers

import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import com.fasterxml.jackson.dataformat.yaml.snakeyaml.error.MarkedYAMLException
import com.fasterxml.jackson.dataformat.yaml.snakeyaml.error.YAMLException
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.io.StringWriter

@QuarkusTest
class YamlAnchorCustomizerTest {
    @Inject
    lateinit var mapper: YAMLMapper

    @Test
    fun `Normal YAML should still be parsed as normal`() {
        val data = SimpleSerializable("foobar", 123)
        val expectedYaml = """
            ---
            foo: "foobar"
            bar: 123
            
        """.trimIndent()

        val writer = StringWriter()
        mapper.writeValue(writer, data)
        val yaml = writer.toString()

        Assertions.assertEquals(expectedYaml, yaml)

        val parsed = mapper.readValue(yaml, SimpleSerializable::class.java)
        Assertions.assertEquals(data, parsed)
    }

    @Test
    fun `YAML anchors should be resolved and parseable`() {
        val normalYaml = """
            foo: foobar
            bar: 123
        """.trimIndent()

        val anchorYaml = """
            .template: &anchor "foobar"
            foo: *anchor
            bar: 123
        """.trimIndent()

        val expected = SimpleSerializable("foobar", 123)

        val parsedNormal = mapper.readValue(normalYaml, SimpleSerializable::class.java)
        Assertions.assertEquals(expected, parsedNormal)

        val parsedAnchor = mapper.readValue(anchorYaml, SimpleSerializable::class.java)
        Assertions.assertEquals(expected, parsedAnchor)
    }

    @Test
    fun `YAML merge key should be resolved and parseable`() {
        data class NestedSerializable(
            val simple: SimpleSerializable,
            val complex: SimpleSerializable,
            val list: List<SimpleSerializable>,
        )

        val normalYaml = """
            simple:
              foo: first
              bar: 123
            complex:
              foo: override
              bar: 123
            list:
              - foo: first
                bar: 123
              - foo: first
                bar: 234
        """.trimIndent()

        val anchorYaml = """
            .template: &anchor
              foo: first
            simple: &simpleAnchor
              <<: *anchor
              bar: 123
            complex:
              <<: *simpleAnchor
              foo: override
            list:
              - *simpleAnchor
              - <<: *simpleAnchor
                bar: 234
        """.trimIndent()

        val expected = NestedSerializable(
            simple = SimpleSerializable("first", 123),
            complex = SimpleSerializable("override", 123),
            list = listOf(
                SimpleSerializable("first", 123),
                SimpleSerializable("first", 234),
            )
        )

        val parsedNormal = mapper.readValue(normalYaml, NestedSerializable::class.java)
        Assertions.assertEquals(expected, parsedNormal)

        val parsedAnchor = mapper.readValue(anchorYaml, NestedSerializable::class.java)
        Assertions.assertEquals(expected, parsedAnchor)
    }

    @Test
    fun `YAML Anchors are not vulnerable to the billion laughs attack`() {
        // From: https://en.wikipedia.org/wiki/Billion_laughs_attack#Variations
        val billionLaughs = """
            a: &a ["lol","lol","lol","lol","lol","lol","lol","lol","lol"]
            b: &b [*a,*a,*a,*a,*a,*a,*a,*a,*a]
            c: &c [*b,*b,*b,*b,*b,*b,*b,*b,*b]
            d: &d [*c,*c,*c,*c,*c,*c,*c,*c,*c]
            e: &e [*d,*d,*d,*d,*d,*d,*d,*d,*d]
            f: &f [*e,*e,*e,*e,*e,*e,*e,*e,*e]
            g: &g [*f,*f,*f,*f,*f,*f,*f,*f,*f]
            h: &h [*g,*g,*g,*g,*g,*g,*g,*g,*g]
            i: &i [*h,*h,*h,*h,*h,*h,*h,*h,*h]
        """.trimIndent()

        Assertions.assertThrows(YAMLException::class.java) { mapper.readTree(billionLaughs) }
    }

    @Test
    fun `Invalid YAML should fail like normal`() {
        // The custom YAML treatment should not change the "exception API"
        // for input that is invalid YAML.
        val invalidYaml = """
            foo: foobar
            invalid
        """.trimIndent()

        Assertions.assertThrowsExactly(MarkedYAMLException::class.java) {
            mapper.readValue(invalidYaml, SimpleSerializable::class.java)
        }
    }

    @Test
    fun `Non-deserializable YAML should fail like normal`() {
        // The custom YAML treatment should not change the "exception API"
        // for input that is valid YAML, but cannot be deserialized.
        val nonDeserializableYaml = """
            foo: foobar
            bar: foo
            bar: 123
        """.trimIndent()

        Assertions.assertThrowsExactly(InvalidFormatException::class.java) {
            mapper.readValue(nonDeserializableYaml, SimpleSerializable::class.java)
        }
    }

    data class SimpleSerializable(
        val foo: String,
        val bar: Int,
    )
}
