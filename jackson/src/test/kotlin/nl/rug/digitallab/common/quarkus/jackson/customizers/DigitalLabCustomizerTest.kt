package nl.rug.digitallab.common.quarkus.jackson.customizers

import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.Duration as JavaDuration
import kotlin.time.Duration as KotlinDuration

@QuarkusTest
class DigitalLabCustomizerTest {
    @Inject
    lateinit var mapper: ObjectMapper

    @Test
    fun `Integer-valued durations should be parsed as milliseconds`() {
        val expected = JavaDuration.ofMinutes(2)
        val milliseconds = expected.toMillis().toString()

        val actual = mapper.readValue(milliseconds, JavaDuration::class.java)

        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `String-valued durations should be serialized and deserialized properly`() {
        // This asserts we keep the normal behaviour
        val duration = JavaDuration.ofMinutes(2)
        val durationString = "\"PT2M\""

        val parsed = mapper.readValue(durationString, JavaDuration::class.java)
        val serialized = mapper.writeValueAsString(parsed)

        Assertions.assertEquals(duration, parsed)
        Assertions.assertEquals(durationString, serialized)
    }
    
    @Test
    fun `Kotlin durations should be serialized and deserialized properly`() {
        val duration = KotlinDuration.parse("PT2M")
        val durationString = "\"PT2M\""
        
        val parsed = mapper.readValue(durationString, KotlinDuration::class.java)
        val serialized = mapper.writeValueAsString(parsed)
        
        Assertions.assertEquals(duration, parsed)
        Assertions.assertEquals(durationString, serialized)
    }
}
