package nl.rug.digitallab.common.quarkus.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
class ProducerTest {
    @Inject lateinit var mapper: ObjectMapper

    @Test
    fun `The default mapper should be a JSON mapper`() {
        Assertions.assertInstanceOf(JsonMapper::class.java, mapper)
    }

    @Inject lateinit var jsonMapper: JsonMapper
    @Inject lateinit var jsonMapper2: JsonMapper
    @Inject lateinit var yamlMapper: YAMLMapper
    @Inject lateinit var yamlMapper2: YAMLMapper
    @Inject lateinit var csvMapper: CsvMapper
    @Inject lateinit var csvMapper2: CsvMapper

    @Test
    fun `Mappers are singletons`() {
        Assertions.assertSame(jsonMapper, jsonMapper2)
        Assertions.assertSame(yamlMapper, yamlMapper2)
        Assertions.assertSame(csvMapper, csvMapper2)
    }

    @Test
    fun `The default mapper and JSON mapper are the same singleton`() {
        Assertions.assertSame(jsonMapper, mapper)
    }
}
